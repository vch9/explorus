# JSON

This folder contains all the configuration file to customize explorus.

-   `components_states.json` is used to activate or deactivate any explorus' component.
-   `known_bakers.json` is used to register every baker that has a more readable and recognizable
    alias.
-   `parameters.json` is used to set networks parameters, in particular tezos node API/addresses,
    this is the file that you need to edit to set your own nodes.
-   `protocol_transitions.json` is used to register every latest protocol transition level of every
    compatible network.
-   `test_network_parameters.json` is used to set all the necessary test network parameters to
    execute all tests properly.
