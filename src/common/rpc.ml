(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

exception Encoding_error of string

module EzReq_lwt = struct
  include EzReq_lwt

  let get3 ?post ?headers ?params ?msg api service arg1 arg2 arg3 =
    request ?headers ?params ?msg ?post ~input:() api service
      (((EzAPI.Req.dummy, arg1), arg2), arg3)

  let get4 ?post ?headers ?params ?msg api service arg1 arg2 arg3 arg4 =
    request ?headers ?params ?msg ?post ~input:() api service
      ((((EzAPI.Req.dummy, arg1), arg2), arg3), arg4)
end

let assert_unknown_error ~__FUNCTION__ ~msg err =
  Lib_js.log_ez_req_lwt_err ~verbose:(__FUNCTION__ ^ msg) err ;
  match err with
  | EzReq_lwt_S.UnknownError { code; msg } when code = -3 ->
    raise
      (Encoding_error
         (Printf.sprintf "During the call to %s : %s" __FUNCTION__
            (Option.value ~default:"no_error_msg" msg)))
  | EzReq_lwt_S.KnownError { error; _ } -> raise error
  | _ -> ()

let verbose msg =
  if Central_state.Debug.verbose () = 10 then
    Some msg
  else
    None

module Process = struct
  let get_fitness_round = function
    | Genesis | Emmy _ -> 0
    | Tenderbake { ft_current_round; _ } -> ft_current_round.dh_dec

  let get_round block = get_fitness_round block.bl_header.hd_fitness

  let get_head_round block = get_fitness_round block.mh_fitness

  let get_level_round block =
    let level = block.bl_header.hd_level in
    let round = get_round block in
    Tezos.Entities.LevelRound.{ level; round }

  let get_head_level_round block =
    let level = block.mh_level in
    let round = get_head_round block in
    Tezos.Entities.LevelRound.{ level; round }

  let get_timestamp block =
    Lib_tz.parse_tezos_timestamp block.bl_header.hd_timestamp
end

(* =================================== L1 =================================== *)

module BlockId = struct
  type t =
    | Hash of string
    | Level of int

  let to_string = function
    | Hash h -> h
    | Level level -> Int.to_string level
end

module BId = BlockId

let get_address_validity_test ?bid address =
  let bid = Option.value ~default:(Central_state.Mode.BState.bid ()) bid in
  let msg = verbose "block" in
  match%lwt
    EzReq_lwt.get1 ?msg (EzAPI.BASE address) Tezos.RPC.Services.block bid
  with
  | Ok _received -> Lwt.return_true
  | Error _not_received -> Lwt.return_false

let rec get_block ?bid () =
  match bid with
  | None ->
    (* @todo : as long as 'Rolled' mode is not activated we know that None case
       is head *)
    let bid = Central_state.Mode.BState.bid () in
    Cache.LevelRoundBlocks.find_head ~default:(get_block_by_state ~bid)
      (ConcurrentValue.get Central_state.Cache.lr_blocks)
  | Some (BId.Hash bid) ->
    Cache.HashBlocks.get Central_state.Cache.hash_blocks bid
      ~search_for:(get_block_timestamped ~bid)
  | Some (BId.Level level as bid) ->
    Log.log
      ("WARNING : Try to not get block by level: " ^ __FUNCTION__ ^ " "
     ^ Int.to_string level) ;
    get_block_by_state ~bid:(BId.to_string bid) ()

and get_block_by_state ~bid () =
  let%lwt block = rpc_block ~bid () in
  let%lwt () = update_caches block in
  Lwt.return block

and get_block_timestamped ~bid () =
  let%lwt block = rpc_block ~bid () in
  let default_ts = Process.get_timestamp block in
  let%lwt () = update_caches ~default_ts block in
  Lwt.return (block, default_ts)

and rpc_block ~bid () =
  let%lwt api = Central_state.API.api () in
  let msg = verbose "block" in
  match%lwt
    EzReq_lwt.get1 ?msg (EzAPI.BASE api) Tezos.RPC.Services.block bid
  with
  | Ok block -> Lwt.return block
  | Error err ->
    assert_unknown_error ~__FUNCTION__ ~msg:(" : cannot get block " ^ bid) err ;
    let%lwt () = Central_state.API.State.quarantine () in
    rpc_block ~bid ()

and update_caches ?default_ts block =
  let default_ts =
    Option.value_delayed
      ~default:(fun () -> Process.get_timestamp block)
      default_ts in
  let%lwt () =
    ConcurrentValue.update Central_state.Cache.lr_blocks @@ fun lr_blocks ->
    Lwt.return
    @@ Cache.LevelRoundBlocks.update
         (Process.get_level_round block)
         block lr_blocks in
  let () =
    Cache.HashBlocks.add ~default_ts Central_state.Cache.hash_blocks
      block.bl_hash block in
  Lwt.return_unit

let rec get_delegates ?bid ?active () =
  let bid = Option.value ~default:(Central_state.Mode.BState.bid ()) bid in
  let active = Option.value ~default:true active in
  let%lwt api = Central_state.API.api () in
  let params =
    let open Tezos.RPC.Services.Params in
    let active = make_active_param ~active () in
    get_params [active] in
  let msg = verbose "delegates" in
  match%lwt
    EzReq_lwt.get1 ~params ?msg (EzAPI.BASE api) Tezos.RPC.Services.delegates
      bid
  with
  | Ok delegates -> Lwt.return delegates
  | Error err ->
    assert_unknown_error ~__FUNCTION__
      ~msg:(" : cannot get delegates from block " ^ bid)
      err ;
    let%lwt () = Central_state.API.State.quarantine () in
    get_delegates ~bid ~active ()

let rec get_participation ?bid delegate =
  let bid = Option.value ~default:(Central_state.Mode.BState.bid ()) bid in
  let%lwt api = Central_state.API.api () in
  let msg = verbose "participation" in
  match%lwt
    EzReq_lwt.get2 ?msg (EzAPI.BASE api) Tezos.RPC.Services.participation bid
      delegate
  with
  | Ok participation -> Lwt.return participation
  | Error err ->
    assert_unknown_error ~__FUNCTION__
      ~msg:
        (" : cannot get participation from block " ^ bid ^ " and delegates : "
       ^ delegate)
      err ;
    let%lwt () = Central_state.API.State.quarantine () in
    get_participation ~bid delegate

let get_participations ?bid delegates =
  let bid = Option.value ~default:(Central_state.Mode.BState.bid ()) bid in
  Lwt_list.map_p (get_participation ~bid) delegates

let get_alias known_bakers account =
  (* @todo : can be optimized *)
  List.assoc_opt account known_bakers

let get_alias_lwt account =
  (* @todo : can be optimized *)
  Lwt.map (List.assoc_opt account) Central_state.DJson.known_bakers

let rec get_delegate_info ?bid delegate =
  let bid = Option.value ~default:(Central_state.Mode.BState.bid ()) bid in
  let%lwt api = Central_state.API.api () in
  let msg = verbose "delegate_info" in
  match%lwt
    EzReq_lwt.get2 ?msg (EzAPI.BASE api) Tezos.RPC.Services.delegate_info bid
      delegate
  with
  | Ok delegate_info -> Lwt.return delegate_info
  | Error err ->
    assert_unknown_error ~__FUNCTION__
      ~msg:
        (" : cannot get delegate_info from block " ^ bid ^ " and delegates : "
       ^ delegate)
      err ;
    let%lwt () = Central_state.API.State.quarantine () in
    get_delegate_info ~bid delegate

let get_delegate_infos ?bid delegates =
  let bid = Option.value ~default:(Central_state.Mode.BState.bid ()) bid in
  Lwt_list.map_p (get_delegate_info ~bid) delegates

let rec get_validators level =
  (* we handle genesis block *)
  if level = 0 then
    Lwt.return_nil
  else
    let bid = Central_state.Mode.BState.bid () in
    let%lwt api = Central_state.API.api () in
    let params =
      let open Tezos.RPC.Services.Params in
      let level = make_level_param ~level () in
      get_params [level] in
    let msg = verbose "validator" in
    match%lwt
      EzReq_lwt.get1 ~params ?msg (EzAPI.BASE api) Tezos.RPC.Services.validators
        bid
    with
    | Ok validators ->
      let%lwt () =
        ConcurrentValue.update Central_state.Cache.validators
        @@ fun validators_cache ->
        Lwt.return @@ Cache.Validators.update level validators validators_cache
      in
      Lwt.return validators
    | Error err ->
      assert_unknown_error ~__FUNCTION__
        ~msg:(" : cannot get validators from level " ^ Int.to_string level)
        err ;
      let%lwt () = Central_state.API.State.quarantine () in
      get_validators level

let rec get_network_constants ?bid () =
  let bid = Option.value ~default:(Central_state.Mode.BState.bid ()) bid in
  let%lwt api = Central_state.API.api () in
  let msg = verbose "network_constants" in
  match%lwt
    EzReq_lwt.get1 ?msg (EzAPI.BASE api) Tezos.RPC.Services.network_constants
      bid
  with
  | Ok network_constants -> Lwt.return network_constants
  | Error err ->
    assert_unknown_error ~__FUNCTION__
      ~msg:(" cannot get network_constants from block " ^ bid)
      err ;
    let%lwt () = Central_state.API.State.quarantine () in
    Central_state.API.State.abort () ;
    get_network_constants ~bid ()

let get_network_constants_path ?bid () =
  let bid = Option.value ~default:(Central_state.Mode.BState.bid ()) bid in
  let%lwt api = Central_state.API.api () in
  let (EzAPI.Url.URL path) =
    EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.network_constants bid []
  in
  Lwt.return path

let rec get_node_version () =
  let%lwt api = Central_state.API.api () in
  let msg = verbose "network_constants" in
  match%lwt
    EzReq_lwt.get0 ?msg (EzAPI.BASE api) Tezos.RPC.Services.node_version
  with
  | Ok node_version -> Lwt.return node_version
  | Error err ->
    let%lwt () = Central_state.API.State.quarantine () in
    Lib_js.log_ez_req_lwt_err
      ~verbose:(__FUNCTION__ ^ " cannot get node_version")
      err ;
    get_node_version ()

let get_block_operations_by_index_path ~bid ~index =
  let%lwt api = Central_state.API.api () in
  let (EzAPI.Url.URL path) =
    EzAPI.forge2 (EzAPI.BASE api) Tezos.RPC.Services.block_operations_by_index
      (BId.to_string bid) index [] in
  Lwt.return path

let get_participation_and_ts delegate () =
  let%lwt participation = get_participation delegate in
  let timestamp = Time.universal () in
  Lwt.return (participation, timestamp)

let get_delegate_info_and_ts delegate () =
  let%lwt delegate_info = get_delegate_info delegate in
  let timestamp = Time.universal () in
  Lwt.return (delegate_info, timestamp)

(* ================================= STREAM ================================= *)

module Stream = struct
  module type Service = sig
    type data

    val data_enc : data Json_encoding.encoding

    type args

    val service : (args, unit, data list, exn, EzAPI.no_security) EzAPI.service
  end

  module type S = sig
    type data

    type args

    type t = {
      listener : data list Lib.Listener.t;
      run_stream :
        api:string ->
        ?params:(EzAPI.Param.t * EzAPI.param_value) list ->
        args:args ->
        unit ->
        unit Lwt.t;
    }

    val create : unit -> t
  end

  module Make (S : Service) : S with type data = S.data and type args = S.args =
  struct
    type data = S.data

    type args = S.args

    type t = {
      listener : data list Listener.t;
      run_stream :
        api:string ->
        ?params:(EzAPI.Param.t * EzAPI.param_value) list ->
        args:S.args ->
        unit ->
        unit Lwt.t;
    }

    let make_path ~api ?(params = []) ~args () =
      let (EzAPI.Url.URL path) =
        EzAPI.forge (EzAPI.BASE api) S.service args params in
      path

    let fold ~listener stream_storage new_chunk =
      let IJ_Parser.{ current_stream; recognizable_json } =
        IJ_Parser.parse_monitor_operation (stream_storage ^ new_chunk) in
      let data : data list =
        List.map (EzEncoding.destruct S.data_enc) recognizable_json in
      Listener.trigger listener data ;
      current_stream

    let create () =
      let listener = Listener.create () in
      let init = "" in
      let fold = fold ~listener in
      let run_stream ~api ?params ~args () =
        let path = make_path ~api ?params ~args () in
        let%lwt _ = Lib_js.run_stream ~path fold init in
        Lwt.return_unit in
      { listener; run_stream }
  end
end

module HDStream : sig
  (** This listener will be triggered each time the node has received new head. *)
  val listener : monitor_heads Lib.Listener.t

  val open_stream : unit -> unit
end = struct
  include Stream.Make (struct
    type data = monitor_head

    let data_enc = monitor_head_enc

    type args = EzAPI.Req.t

    let service = Tezos.RPC.Services.monitor_heads
  end)

  let { listener; run_stream } = create ()

  let open_stream () =
    let args = EzAPI.Req.dummy in
    let rec aux () =
      let%lwt api = Central_state.API.api () in
      let%lwt () =
        try%lwt run_stream ~api ~args ()
        with Lib_js.Stream_error err ->
          if Central_state.Debug.verbose () = 2 then
            Log.log (__FUNCTION__ ^ " error : " ^ err) ;
          Central_state.API.State.quarantine () in
      aux () in
    Lwt.async aux
end

module MOStream : sig
  module Applied : sig
    (** This listener will be triggered each time the node has received applied
        operations. *)
    val listener : monitor_operations Lib.Listener.t

    val open_stream : unit -> unit
  end

  module Outdated : sig
    (** This listener will be triggered each time the node has received outdated
        operations. *)
    val listener : monitor_operations Lib.Listener.t

    val open_stream : unit -> unit
  end
end = struct
  include Stream.Make (struct
    type data = monitor_operation

    let data_enc = monitor_operation_enc

    type args = EzAPI.Req.t

    let service = Tezos.RPC.Services.monitor_operations
  end)

  let open_stream
      ~(run_stream : api:_ -> ?params:_ -> args:_ -> unit -> unit Lwt.t)
      ?applied ?outdated () =
    let params =
      let open Tezos.RPC.Services.Params in
      let applied = make_applied_param ?applied () in
      let outdated = make_outdated_param ?outdated () in
      get_params [applied; outdated] in
    let args = EzAPI.Req.dummy in
    ignore
    @@ Listener.connect HDStream.listener
    @@ fun _ ->
    Lwt.async @@ fun () ->
    let%lwt api = Central_state.API.api () in
    try%lwt run_stream ~api ~params ~args ()
    with Lib_js.Stream_error err ->
      if Central_state.Debug.verbose () = 2 then
        Log.log (__FUNCTION__ ^ " error : " ^ err) ;
      Central_state.API.State.quarantine ()

  module Applied = struct
    let { listener; run_stream } = create ()

    let open_stream () =
      open_stream ~run_stream ~applied:true ~outdated:false ()
  end

  module Outdated = struct
    let { listener; run_stream } = create ()

    let open_stream () =
      open_stream ~run_stream ~applied:false ~outdated:true ()
  end
end

module SORU = struct
  module L1 = struct
    let rec get_genesis_info rollup_id =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.genesis_info
          rollup_id
      with
      | Ok genesis_info -> Lwt.return genesis_info
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:(" : cannot get soru genesis info from " ^ rollup_id)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_genesis_info rollup_id

    let rec get_inbox () =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get0 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.inbox
      with
      | Ok inbox -> Lwt.return inbox
      | Error err ->
        assert_unknown_error ~__FUNCTION__ ~msg:" : cannot get inbox" err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_inbox ()

    let rec get_last_cemented_commitment_hash_with_level rollup_id =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get1 (EzAPI.BASE api)
          Tezos.RPC.Services.SORU.L1.last_cemented_commitment_hash_with_level
          rollup_id
      with
      | Ok last_cemented_commitment_hash_with_level ->
        Lwt.return last_cemented_commitment_hash_with_level
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:
            (" : cannot get soru last cemented commitment hash with level from "
           ^ rollup_id)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_last_cemented_commitment_hash_with_level rollup_id

    let rec get_commitment_infos rollup_id commitment_hash =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get2 (EzAPI.BASE api)
          Tezos.RPC.Services.SORU.L1.commitment_infos rollup_id commitment_hash
      with
      | Ok commitment_infos -> Lwt.return commitment_infos
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:(" : cannot get soru commitment infos from " ^ rollup_id)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_commitment_infos rollup_id commitment_hash

    let rec get_commitments rollup_id inbox_level =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.commitments
          rollup_id inbox_level
      with
      | Ok commitments -> Lwt.return commitments
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:
            (" : cannot get soru commitments from " ^ rollup_id
           ^ " at inbox level " ^ Int.to_string inbox_level)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_commitments rollup_id inbox_level

    let rec get_stakers_ids rollup_id commitment =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.stakers_ids
          rollup_id commitment
      with
      | Ok stakers_ids -> Lwt.return stakers_ids
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:
            (" : cannot get soru stakers indexes from " ^ rollup_id
           ^ " for commitment " ^ commitment)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_stakers_ids rollup_id commitment

    let rec get_staker_id rollup_id staker =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.staker_id
          rollup_id staker
      with
      | Ok staker_id -> Lwt.return staker_id
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:
            (" : cannot get soru staker index from " ^ rollup_id
           ^ " for staker " ^ staker)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_staker_id rollup_id staker

    let rec get_stakers rollup_id =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.stakers
          rollup_id
      with
      | Ok stakers -> Lwt.return stakers
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:(" : cannot get soru stakers " ^ rollup_id)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_stakers rollup_id

    let rec get_soru_addresses () =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get0 (EzAPI.BASE api)
          Tezos.RPC.Services.SORU.L1.soru_addresses
      with
      | Ok soru_addresses -> Lwt.return soru_addresses
      | Error err ->
        assert_unknown_error ~__FUNCTION__ ~msg:" : cannot get soru addresses"
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_soru_addresses ()

    let rec get_kind rollup_id =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.kind
          rollup_id
      with
      | Ok kind -> Lwt.return kind
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:(" : cannot get soru kind from " ^ rollup_id)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_kind rollup_id

    let get_id_validity_test rollup_id =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.kind
          rollup_id
      with
      | Ok _kind -> Lwt.return_true
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:(" : cannot get soru kind from " ^ rollup_id)
          err ;
        Lwt.return_false

    let rec get_staked_on_commitment rollup_id staker =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get2 (EzAPI.BASE api)
          Tezos.RPC.Services.SORU.L1.staked_on_commitment rollup_id staker
      with
      | Ok commitment -> Lwt.return commitment
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:
            (" : cannot get the staked on commitment from " ^ rollup_id
           ^ " for " ^ staker)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_staked_on_commitment rollup_id staker

    let rec get_conflicts rollup_id staker =
      let%lwt api = Central_state.API.api () in
      match%lwt
        EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.conflicts
          rollup_id staker
      with
      | Ok conflicts -> Lwt.return conflicts
      | Error err ->
        assert_unknown_error ~__FUNCTION__
          ~msg:
            (" : cannot get soru conflicts from " ^ rollup_id ^ " for " ^ staker)
          err ;
        let%lwt () = Central_state.API.State.quarantine () in
        get_conflicts rollup_id staker

    let get_ongoing_refutation_games ?bid rollup_id staker =
      let bid =
        Option.perform_delayed bid ~default:Central_state.Mode.BState.bid
          ~perform:BId.to_string in
      let rec aux () =
        let%lwt api = Central_state.API.api () in
        match%lwt
          EzReq_lwt.get3 (EzAPI.BASE api)
            Tezos.RPC.Services.SORU.L1.ongoing_refutation_games bid rollup_id
            staker
        with
        | Ok games -> Lwt.return games
        | Error err ->
          assert_unknown_error ~__FUNCTION__
            ~msg:
              (" : cannot get soru games from " ^ rollup_id ^ " for " ^ staker)
            err ;
          let%lwt () = Central_state.API.State.quarantine () in
          aux () in
      aux ()

    let get_timeout rollup_id ?bid staker1 staker2 =
      let bid =
        Option.perform_delayed bid ~default:Central_state.Mode.BState.bid
          ~perform:BId.to_string in
      let rec aux () =
        let%lwt api = Central_state.API.api () in
        match%lwt
          EzReq_lwt.get4 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.timeout bid
            rollup_id staker1 staker2
        with
        | Ok timeout -> Lwt.return timeout
        | Error err ->
          assert_unknown_error ~__FUNCTION__
            ~msg:
              (" : cannot get soru timeout from " ^ rollup_id ^ " between "
             ^ staker1 ^ " and " ^ staker2)
            err ;
          let%lwt () = Central_state.API.State.quarantine () in
          aux () in
      aux ()
  end
end
