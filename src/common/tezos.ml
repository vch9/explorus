(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
[@@@ocamlformat "wrap-comments=false"]
(* @@ File overview :
   ==> RPC
   ------> Types
   ..................> SORU
   ------> Services
   ..................> Params
   ..................> SORU
   ==> Entities
   ------> Endorsers *)

module Encoding = struct
  open Json_encoding

  let ignore_enc enc =
    let destruct x = (x, ()) in
    let construct (x, ()) = x in
    let enc = merge_objs enc unit in
    conv destruct construct enc

  let float_of_string_enc = conv Float.to_string Float.of_string string

  let int_of_string_enc = conv string_of_int int_of_string string

  let int64_of_string_enc = conv Int64.to_string Int64.of_string string

  let singleton_enc enc =
    let open Json_encoding in
    conv
      (fun x -> [x])
      (function
        | [x] -> x
        | _ -> raise (Cannot_destruct ([], Failure "is not a singleton")))
      (list enc)
end

module RPC = struct
  module Types = struct
    module SORU = struct
      module L1 = struct
        type ('content, 'ptr) skip_list = {
          sl_content : 'content;
          sl_back_pointers : 'ptr list;
          sl_index : int; [@encoding Encoding.int_of_string_enc]
        }
        [@@deriving jsoo, encoding]

        type genesis_info = {
          gi_level : int;
          gi_commitment_hash : string;
        }
        [@@deriving jsoo, encoding { ignore }]

        type level_proof = {
          lp_hash : string;
          lp_level : int;
        }
        [@@deriving jsoo, encoding { ignore }]

        type old_levels_messages = (level_proof, string) skip_list
        [@@deriving jsoo, encoding]

        type rollups_inbox = {
          ri_level : int;
          ri_old_levels_messages : old_levels_messages;
        }
        [@@deriving jsoo, encoding { ignore }]

        type commitment_infos = {
          compressed_state : string;
          inbox_level : int;
          predecessor : string;
          number_of_ticks : string;
        }
        [@@deriving jsoo, encoding { ignore }]

        type commitment_infos_with_hash = {
          cih_info : commitment_infos; [@merge]
          cih_hash : string;
        }
        [@@deriving jsoo, encoding]

        type soru_addresses = string list [@@deriving jsoo, encoding]

        type commitment = string [@@deriving jsoo, encoding]

        type commitments = commitment list option [@@deriving jsoo, encoding]

        type staker = string [@@deriving jsoo, encoding]

        type stakers = staker list [@@deriving jsoo, encoding]

        type staker_id = string [@@deriving jsoo, encoding]

        type stakers_ids = staker_id list [@@deriving jsoo, encoding]

        type kind = string [@@deriving jsoo, encoding]

        type conflict = {
          c_other : staker;
          c_their_commitment : commitment_infos;
          c_our_commitment : commitment_infos;
          c_parent_commitment : commitment;
        }
        [@@deriving jsoo, encoding]

        type conflicts = conflict list [@@deriving jsoo, encoding]

        type timeout = {
          t_alice : int;
          t_bob : int;
          t_last_turn_level : int;
        }
        [@@deriving jsoo, encoding]

        type timeout_opt = timeout option [@@deriving jsoo, encoding]

        type player =
          | Alice
          | Bob
        [@@deriving jsoo, encoding]

        type game_dissection_chunk = {
          gdc_state : string option;
          gdc_tick : int; [@encoding Encoding.int_of_string_enc]
        }
        [@@deriving jsoo, encoding]

        type game_dissecting = {
          gd_dissection : game_dissection_chunk list;
          gd_number_of_sections : int; [@key "default_number_of_sections"]
        }
        [@@deriving jsoo, encoding]

        type game_final_move = {
          gfm_agreed_start_chunk : game_dissection_chunk;
          gfm_refuted_stop_chunk : game_dissection_chunk;
        }
        [@@deriving jsoo, encoding]

        type game_state =
          | Dissecting of game_dissecting [@kind "Dissecting"]
          | Final_move of game_final_move [@kind "Final_move"]
        [@@deriving jsoo, encoding]

        type inbox_history_proof = (level_proof, string) skip_list
        [@@deriving jsoo, encoding]

        type dal_id = {
          di_published_level : int; [@key "level"]
          di_index : int;
        }
        [@@deriving jsoo, encoding]

        type dal_header = {
          dh_id : dal_id; [@merge]
          dh_commitment : commitment;
        }
        [@@deriving jsoo, encoding]

        type dal_history = (dal_header, string) skip_list
        [@@deriving jsoo, encoding]

        type game = {
          g_turn : player;
          g_inbox_snapshot : inbox_history_proof;
          g_dal_snapshot : dal_history;
          g_start_level : int;
          g_inbox_level : int;
          g_game_state : game_state;
        }
        [@@deriving jsoo, encoding]

        type refutation_game = {
          rg_game : game;
          rg_alice : staker;
          rg_bob : staker;
        }
        [@@deriving jsoo, encoding]

        type refutation_games = refutation_game list [@@deriving jsoo, encoding]

        type refutation_game_players = {
          rgp_alice : staker;
          rgp_bob : staker;
        }
        [@@deriving jsoo, encoding]

        type refutation_start = {
          rs_player_commitment_hash : commitment;
          rs_opponent_commitment_hash : commitment;
        }
        [@@deriving jsoo, encoding { ignore }]

        type refutation_proof = { pvm_step : string }
        [@@deriving jsoo, encoding { ignore }]

        type refutation_step =
          | Dissection of game_dissection_chunk list
          | Proof of refutation_proof
        [@@deriving jsoo, encoding]

        type refutation_move = {
          rm_choice : int; [@encoding Encoding.int_of_string_enc]
          rm_step : refutation_step;
        }
        [@@deriving jsoo, encoding { ignore }]

        type refutation =
          | Start of refutation_start [@kind] [@kind_label "refutation_kind"]
          | Move of refutation_move [@kind] [@kind_label "refutation_kind"]
        [@@deriving jsoo, encoding { ignore }]

        type game_lost_reason =
          | Conflict_resolved
          | Timeout
        [@@deriving jsoo { enum }, encoding]

        type game_result =
          | Loser of {
              reason : game_lost_reason;
              player : staker;
            } [@kind]
          | Draw [@kind]
        [@@deriving jsoo, encoding]

        type game_status =
          | Ongoing
          | Ended of game_result [@wrap "result"]
        [@@deriving jsoo, encoding]

        type refutation_game_metadata = { game_status : game_status }
        [@@deriving jsoo, encoding { ignore }]

        module Cosmetics = struct
          type inbox_level = int

          type commitments = string list
        end

        module C = Cosmetics
      end
    end

    type level_info = {
      cycle : int;
      cycle_position : int;
    }
    [@@deriving
      jsoo { remove_prefix = false }, encoding { ignore; remove_prefix = false }]

    type dec_hex = {
      dh_dec : int;
      dh_hex : string;
    }
    [@@deriving jsoo]

    type dec64_hex = {
      dh64_dec : int64;
      dh64_hex : string;
    }
    [@@deriving jsoo]

    let dec_hex_enc =
      let open Json_encoding in
      let destruct { dh_hex; _ } = dh_hex in
      let construct dh_hex =
        try { dh_dec = Hex.to_int dh_hex; dh_hex }
        with exn -> raise (Json_encoding.Cannot_destruct ([], exn)) in
      conv destruct construct string

    let dec_hex_enc_opt =
      let open Json_encoding in
      let destruct = function
        | None -> ""
        | Some { dh_hex; _ } -> dh_hex in
      let construct dh_hex =
        try Some { dh_dec = Hex.to_int dh_hex; dh_hex } with _ -> None in
      conv destruct construct string

    let dec64_hex_enc =
      let open Json_encoding in
      let destruct { dh64_hex; _ } = dh64_hex in
      let construct dh64_hex =
        try { dh64_dec = Hex.to_int64 dh64_hex; dh64_hex }
        with exn -> raise (Json_encoding.Cannot_destruct ([], exn)) in
      conv destruct construct string

    let dec64_hex_enc_opt =
      let open Json_encoding in
      let destruct = function
        | None -> ""
        | Some { dh64_hex; _ } -> dh64_hex in
      let construct dh64_hex =
        try Some { dh64_dec = Hex.to_int64 dh64_hex; dh64_hex } with _ -> None
      in
      conv destruct construct string

    module Fitness_version = struct
      let emmy = "00"

      let emmy_star = "01"

      let tenderbake = "02"
    end

    type tenderbake_fitness = {
      (* current level of block but in hexadecimal *)
      ft_current_level_hexa : dec_hex;
      (* ε if there is no pqc, else case previous round of pqc *)
      ft_previous_round_of_pqc : dec_hex option;
      (* we "prioritize" the oldest previous round *)
      ft_minus_previous_round : dec_hex;
      (* we "prioritize" the freshest current round *)
      ft_current_round : dec_hex;
    }
    [@@deriving jsoo]

    (* Fitness deconstructed into a readable and understandable type *)
    type fitness =
      | Genesis
      | Emmy of dec64_hex
      | Tenderbake of tenderbake_fitness
    [@@deriving jsoo]

    let fitness_enc =
      let open Json_encoding in
      let genesis =
        case ~title:"genesis" (list empty)
          (function
            | Genesis -> Some []
            | _ -> None)
          (fun _ -> Genesis) in
      let emmy =
        case ~title:"emmy"
          (tup2 (constant Fitness_version.emmy) dec64_hex_enc)
          (function
            | Emmy dec64_hex -> Some ((), dec64_hex)
            | _ -> None)
          (fun ((), dec64_hex) -> Emmy dec64_hex) in
      let tenderbake =
        case ~title:"tenderbake"
          (tup5
             (constant Fitness_version.tenderbake)
             dec_hex_enc dec_hex_enc_opt dec_hex_enc dec_hex_enc)
          (function
            | Tenderbake
                {
                  ft_current_level_hexa;
                  ft_previous_round_of_pqc;
                  ft_minus_previous_round;
                  ft_current_round;
                } ->
              Some
                ( (),
                  ft_current_level_hexa,
                  ft_previous_round_of_pqc,
                  ft_minus_previous_round,
                  ft_current_round )
            | _ -> None)
          (fun ( (),
                 ft_current_level_hexa,
                 ft_previous_round_of_pqc,
                 ft_minus_previous_round,
                 ft_current_round ) ->
            Tenderbake
              {
                ft_current_level_hexa;
                ft_previous_round_of_pqc;
                ft_minus_previous_round;
                ft_current_round;
              }) in
      union [genesis; emmy; tenderbake]

    type header = {
      hd_level : int;
      hd_predecessor : string;
      hd_timestamp : string;
      hd_fitness : fitness;
    }
    [@@deriving jsoo, encoding { ignore }]

    (* balance_kind :
       { kind: "contract"; ... } will be [Contract]
       { kind: "minted"; category: "baking bonuses"; ... } will be [Minted
       Baking_bonuses]
       { kind: "minted"; category: ...; ... } will be [Minted
       OtherBalanceMinted]
       { kind: ...; ... } will be [OtherBalanceKind] *)
    type balance_minted =
      | Baking_bonuses [@kind "baking bonuses"] [@kind_label "category"]
      | OtherBalanceMinted of { category : string } [@ignore]
    [@@deriving jsoo, encoding { ignore }]

    type balance_kind =
      | Contract [@kind]
      | Minted of balance_minted [@kind]
      | OtherBalanceKind of { kind : string } [@ignore]
    [@@deriving jsoo, encoding { ignore }]

    type balance_update = {
      bu_kind : balance_kind; [@merge]
      bu_change : float; [@encoding Encoding.float_of_string_enc]
    }
    [@@deriving jsoo, encoding { ignore }]

    type balance_updates = balance_update list [@@deriving jsoo, encoding]

    type metadata = {
      meta_baker : string option;
      meta_proposer : string option;
      meta_level_info : level_info option;
      meta_balance_updates : balance_updates option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type co_content_metadata = {
      cocm_endorsement_power : int option;
      cocm_delegate : string option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type 'successful operation_result =
      | Applied of 'successful [@kind] [@kind_label "status"]
      | OtherOperationResult of { status : string } [@ignore]
    [@@deriving jsoo, encoding]

    type 'successful operation_metadata = {
      operation_result : 'successful operation_result;
    }
    [@@deriving jsoo, encoding { ignore }]

    type consensus_op_content = {
      coc_slot : int;
      coc_level : int;
      coc_round : int;
      coc_metadata : co_content_metadata option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type manager = { man_source : string [@key "source"] }
    [@@deriving jsoo, encoding { ignore }]

    type parameters = { entrypoint : string }
    [@@deriving jsoo, encoding { ignore }]

    type contract_transaction = {
      ctr_manager : manager; [@merge]
      ctr_amount : string;
      ctr_parameters : parameters;
      ctr_destination : string;
    }
    [@@deriving jsoo, encoding { ignore }]

    let contract_transaction_enc =
      let destruct transaction = transaction in
      let construct ({ ctr_destination; _ } as transaction) =
        if String.starts_with ~prefix:"KT" ctr_destination then
          transaction
        else
          raise
            (Json_encoding.Cannot_destruct
               ([], Failure "The destination is not a contract")) in
      Json_encoding.conv destruct construct contract_transaction_enc

    type user_transaction = {
      utr_manager : manager; [@merge]
      utr_amount : string;
      utr_destination : string;
    }
    [@@deriving jsoo, encoding { ignore }]

    let user_transaction_enc =
      let destruct transaction = transaction in
      let construct transaction =
        if not @@ String.starts_with ~prefix:"tz" transaction.utr_destination
        then
          raise
            (Json_encoding.Cannot_destruct
               ([], Failure "The destination is not a user"))
        else
          transaction in
      Json_encoding.conv destruct construct user_transaction_enc

    type origination = { or_manager : manager [@key "manager"] [@merge] }
    [@@deriving jsoo, encoding { ignore }]

    type smart_rollup_originate = {
      sro_manager : manager; [@key "manager"] [@merge]
    }
    [@@deriving jsoo, encoding { ignore }]

    type smart_rollup_refute = {
      srr_manager : manager; [@merge]
      srr_rollup : string;
      srr_opponent : string;
      srr_refutation : SORU.L1.refutation;
      srr_metadata : SORU.L1.refutation_game_metadata operation_metadata option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type smart_rollup_timeout = {
      srt_manager : manager; [@merge]
      srt_rollup : string;
      srt_stakers : SORU.L1.refutation_game_players;
      srt_metadata : SORU.L1.refutation_game_metadata operation_metadata option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type operation_content =
      | Preendorsement of consensus_op_content [@kind]
      | Endorsement of consensus_op_content [@kind]
      | Contract_transaction of contract_transaction [@kind "transaction"]
      | User_transaction of user_transaction [@kind "transaction"]
      | Origination of origination [@kind]
      | Smart_rollup_originate of smart_rollup_originate [@kind]
      | Smart_rollup_refute of smart_rollup_refute [@kind]
      | Smart_rollup_timeout of smart_rollup_timeout [@kind]
      | OtherOperationContent of { kind : string } [@ignore]
    [@@deriving jsoo, encoding { ignore }]

    type operation = {
      op_hash : string;
      op_contents : operation_content list;
    }
    [@@deriving jsoo, encoding { ignore }]

    type operations = {
      ops_endorsements : operation list;
      ops_votings : operation list;
      ops_revealations_walletactivations_denunciations : operation list;
      ops_monitor_ops : operation list;
    }
    [@@deriving jsoo]

    let operations_enc =
      let open Json_encoding in
      let destruct
          {
            ops_endorsements;
            ops_votings;
            ops_revealations_walletactivations_denunciations;
            ops_monitor_ops;
          } =
        [
          ops_endorsements;
          ops_votings;
          ops_revealations_walletactivations_denunciations;
          ops_monitor_ops;
        ] in
      let construct = function
        | [
            ops_endorsements;
            ops_votings;
            ops_revealations_walletactivations_denunciations;
            ops_monitor_ops;
          ] ->
          {
            ops_endorsements;
            ops_votings;
            ops_revealations_walletactivations_denunciations;
            ops_monitor_ops;
          }
        | [] ->
          {
            ops_endorsements = [];
            ops_votings = [];
            ops_revealations_walletactivations_denunciations = [];
            ops_monitor_ops = [];
          }
        | _ :: _ :: _ :: _ :: _ :: _ ->
          raise
            (Json_encoding.Cannot_destruct
               ([], Failure "Too many operations list"))
        | _ ->
          raise
            (Json_encoding.Cannot_destruct
               ([], Failure "Not enough operations list")) in
      conv destruct construct (list (list operation_enc))

    type block = {
      bl_protocol : string;
      bl_chain_id : string;
      bl_hash : string;
      bl_header : header;
      bl_metadata : metadata option;
      bl_operations : operations;
    }
    [@@deriving jsoo, encoding { ignore }]

    type delegates = string list [@@deriving jsoo, encoding]

    type participation = {
      pr_expected_cycle_activity : int;
      pr_minimal_cycle_activity : int;
      pr_missed_slots : int;
      pr_missed_levels : int;
      pr_remaining_allowed_missed_slots : int;
      pr_expected_endorsing_rewards : int64;
          [@encoding Encoding.int64_of_string_enc]
    }
    [@@deriving jsoo, encoding { ignore }]

    type participations = participation list [@@deriving jsoo]

    type delegate_info = {
      di_frozen_deposits : int64; [@encoding Encoding.int64_of_string_enc]
      di_staking_balance : int64; [@encoding Encoding.int64_of_string_enc]
      di_delegated_contracts : string list;
      di_deactivated : bool;
      di_grace_period : int;
    }
    [@@deriving jsoo, encoding { ignore }]

    type delegate_infos = delegate_info list [@@deriving jsoo]

    type validator = {
      val_level : int;
      val_delegate : string;
      val_slots : int list;
    }
    [@@deriving jsoo, encoding { ignore }]

    type validators = validator list [@@deriving jsoo, encoding]

    type mo_error = { moe_kind : string } [@@deriving jsoo, encoding { ignore }]

    type monitor_operation = {
      mo_hash : string;
      mo_contents : operation_content list;
      mo_errors : mo_error list option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type monitor_operations = monitor_operation list [@@deriving jsoo, encoding]

    type monitor_head = {
      mh_hash : string;
      mh_level : int;
      mh_timestamp : string;
      mh_receipt_timestamp : float;
      mh_fitness : fitness;
    }
    [@@deriving encoding { ignore }]

    let monitor_head_enc =
      let open Json_encoding in
      let destruct { mh_hash; mh_level; mh_timestamp; mh_fitness; _ } =
        (mh_hash, mh_level, mh_timestamp, mh_fitness) in
      let construct (mh_hash, mh_level, mh_timestamp, mh_fitness) =
        let mh_receipt_timestamp = Time.universal () in
        { mh_hash; mh_level; mh_timestamp; mh_receipt_timestamp; mh_fitness }
      in
      Encoding.ignore_enc
      @@ conv destruct construct
           (obj4 (req "hash" string) (req "level" int) (req "timestamp" string)
              (req "fitness" fitness_enc))

    type monitor_heads = monitor_head list [@@deriving encoding]

    type network_constants = {
      nc_blocks_per_cycle : int;
      nc_baking_reward_fixed_portion : float;
          [@encoding Encoding.float_of_string_enc]
      nc_baking_reward_bonus_per_slot : string;
      nc_consensus_committee_size : int;
      nc_consensus_threshold : int;
      nc_minimal_block_delay : int; [@encoding Encoding.int_of_string_enc]
      nc_delay_increment_per_round : int; [@encoding Encoding.int_of_string_enc]
      nc_smart_rollup_commitment_period_in_blocks : int option;
      nc_smart_rollup_challenge_window_in_blocks : int option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type commit_info = { commit_hash : string }
    [@@deriving jsoo, encoding { ignore }]

    type network_version = { chain_name : string }
    [@@deriving jsoo, encoding { ignore }]

    type node_version = {
      commit_info : commit_info option;
      network_version : network_version;
    }
    [@@deriving jsoo, encoding { ignore }]
  end

  module Error = struct
    type t = { id : string } [@@deriving encoding { ignore }]

    let make_err ~name ~exn ~id =
      EzAPI.Err.make ~code:500 ~name
        ~encoding:(Encoding.singleton_enc @@ enc)
        ~select:(function
          | exn' when exn' = exn -> Some { id }
          | _ -> None)
        ~deselect:(function
          | err when String.ends_with ~suffix:id err.id -> exn
          | _ ->
            raise (Json_encoding.Cannot_destruct ([], Failure "wrong encoding")))

    exception Unknown_seed

    (* example: [{ id: "proto.alpha.seed.unknown_seed"; ... }] *)
    let unknown_seed =
      make_err ~name:"Unknown seed" ~exn:Unknown_seed ~id:"seed.unknown_seed"

    exception Storage_error

    (* example: [{ id: "proto.alpha.context.storage_error"; ... }] *)
    let storage_error =
      make_err ~name:"Storage error" ~exn:Storage_error
        ~id:"context.storage_error"

    exception Smart_rollup_unknown_commitment

    (* example: [{ id: "proto.alpha.smart_rollup_unknown_commitment"; ... }] *)
    let smart_rollup_unknown_commitment =
      make_err ~name:"Smart rollup unknown commitment"
        ~exn:Smart_rollup_unknown_commitment
        ~id:"smart_rollup_unknown_commitment"
  end

  module Services = struct
    module Params = struct
      let active_param = EzAPI.Param.string "active"

      let make_active_param ?active () =
        Option.map (fun active -> (active_param, EzAPI.TYPES.B active)) active

      let level_param = EzAPI.Param.string "level"

      let make_level_param ?level () =
        Option.map (fun level -> (level_param, EzAPI.TYPES.I level)) level

      let applied_param = EzAPI.Param.string "applied"

      let make_applied_param ?applied () =
        Option.map
          (fun applied -> (applied_param, EzAPI.TYPES.B applied))
          applied

      let outdated_param = EzAPI.Param.string "outdated"

      let make_outdated_param ?outdated () =
        Option.map
          (fun outdated -> (outdated_param, EzAPI.TYPES.B outdated))
          outdated

      let get_params params = List.filter_map Compute.identity params
    end

    [@@@get
    {
      name = "block";
      path = "/chains/main/blocks/{blockid : string}";
      output = Types.block_enc;
    }]

    [@@@get
    {
      name = "delegates";
      path = "/chains/main/blocks/{blockid : string}/context/delegates";
      params = Params.[active_param];
      output = Types.delegates_enc;
    }]

    [@@@get
    {
      name = "participation";
      path =
        "/chains/main/blocks/{blockid : string}/context/delegates/{delegate : \
         string}/participation";
      output = Types.participation_enc;
    }]

    [@@@get
    {
      name = "delegate_info";
      path =
        "/chains/main/blocks/{blockid : string}/context/delegates/{delegate : \
         string}";
      output = Types.delegate_info_enc;
    }]

    [@@@get
    {
      name = "validators";
      path = "/chains/main/blocks/{blockid : string}/helpers/validators";
      params = Params.[level_param];
      output = Types.validators_enc;
      errors = Error.[unknown_seed; storage_error];
    }]

    [@@@get
    {
      name = "monitor_operations";
      path = "/chains/main/mempool/monitor_operations";
      params = Params.[applied_param; outdated_param];
      output = Types.monitor_operations_enc;
    }]

    [@@@get
    {
      name = "monitor_heads";
      path = "monitor/heads/main";
      output = Types.monitor_heads_enc;
    }]

    [@@@get
    {
      name = "network_constants";
      path = "/chains/main/blocks/{blockid : string}/context/constants";
      output = Types.network_constants_enc;
    }]

    [@@@get
    {
      name = "node_version";
      path = "/version";
      output = Types.node_version_enc;
    }]

    [@@@get
    {
      name = "block_operations_by_index";
      path = "/chains/main/blocks/{blockid : string}/operations/3/{index : int}";
    }]

    module SORU = struct
      module L1 = struct
        [@@@get
        {
          name = "genesis_info";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/genesis_info";
          output = Types.SORU.L1.genesis_info_enc;
        }]

        [@@@get
        {
          name = "inbox";
          path = "chains/main/blocks/head/context/smart_rollups/all/inbox";
          output = Types.SORU.L1.rollups_inbox_enc;
        }]

        [@@@get
        {
          name = "last_cemented_commitment_hash_with_level";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/last_cemented_commitment_hash_with_level";
          output = Types.SORU.L1.level_proof_enc;
        }]

        [@@@get
        {
          name = "staked_on_commitment";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/staker/{staker_id : string}/staked_on_commitment";
          output =
            Json_encoding.option Types.SORU.L1.commitment_infos_with_hash_enc;
        }]

        [@@@get
        {
          name = "commitment_infos";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/commitment/{commitment_hash : string}";
          output = Types.SORU.L1.commitment_infos_enc;
          errors = Error.[smart_rollup_unknown_commitment];
        }]

        [@@@get
        {
          name = "commitments";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/inbox_level/{inbox_level : int}/commitments";
          output = Types.SORU.L1.commitments_enc;
        }]

        [@@@get
        {
          name = "stakers_ids";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/commitment/{commitment : string}/stakers_indexes";
          output = Types.SORU.L1.stakers_ids_enc;
          errors = Error.[storage_error];
        }]

        [@@@get
        {
          name = "staker_id";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/staker/{staker : string}/index";
          output = Types.SORU.L1.staker_id_enc;
        }]

        [@@@get
        {
          name = "stakers";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/stakers";
          output = Types.SORU.L1.stakers_enc;
        }]

        [@@@get
        {
          name = "conflicts";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/staker/{staker_id : string}/conflicts";
          output = Types.SORU.L1.conflicts_enc;
        }]

        [@@@get
        {
          name = "ongoing_refutation_games";
          path =
            "chains/main/blocks/{blockid : \
             string}/context/smart_rollups/smart_rollup/{smart_rollup_id : \
             string}/staker/{staker_id : string}/games";
          output = Types.SORU.L1.refutation_games_enc;
        }]

        [@@@get
        {
          name = "timeout";
          path =
            "chains/main/blocks/{blockid : \
             string}/context/smart_rollups/smart_rollup/{smart_rollup_id : \
             string}/staker1/{staker1_id : string}/staker2/{staker2_id : \
             string}/timeout";
          output = Types.SORU.L1.timeout_opt_enc;
        }]

        [@@@get
        {
          name = "soru_addresses";
          path = "chains/main/blocks/head/context/smart_rollups/all";
          output = Types.SORU.L1.soru_addresses_enc;
        }]

        [@@@get
        {
          name = "kind";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/kind";
          output = Types.SORU.L1.kind_enc;
        }]
      end
    end
  end
end

module Entities = struct
  module EndoPreendo = struct
    type kind =
      [ `Endorsement
      | `Preendorsement ]

    type 'a t = {
      endorsement : 'a;
      preendorsement : 'a;
    }
    [@@deriving jsoo]

    let init v = { endorsement = v; preendorsement = v }

    let get_with_kind ~(kind : kind) ep =
      match kind with
      | `Endorsement -> ep.endorsement
      | `Preendorsement -> ep.preendorsement

    let map f { endorsement; preendorsement } =
      { endorsement = f endorsement; preendorsement = f preendorsement }

    let map_with_kind ~(kind : kind) f ep =
      match kind with
      | `Endorsement -> { ep with endorsement = f ep.endorsement }
      | `Preendorsement -> { ep with preendorsement = f ep.preendorsement }

    let merge merge ep1 ep2 =
      {
        endorsement = merge ep1.endorsement ep2.endorsement;
        preendorsement = merge ep1.preendorsement ep2.preendorsement;
      }

    let iter (f : kind:kind -> 'a -> unit) { endorsement; preendorsement } =
      f ~kind:`Endorsement endorsement ;
      f ~kind:`Preendorsement preendorsement

    let fold (f : kind:kind -> 'a -> 'b -> 'b) { endorsement; preendorsement }
        (acc : 'b) =
      let acc = f ~kind:`Endorsement endorsement acc in
      let acc = f ~kind:`Preendorsement preendorsement acc in
      acc
  end

  module Endorser = String
  module EndorserSet = Set.Make (Endorser)

  module Baker = struct
    type t = string [@@deriving jsoo]

    let compare = String.compare
  end

  module BakerMap = Map.Make (Baker)

  module Validator = struct
    type t = RPC.Types.validator

    let compare = compare
  end

  module ValidatorSet = Set.Make (Validator)

  module Hash = struct
    type t = string [@@deriving jsoo]

    let compare = String.compare
  end

  module HashMap = Map.Make (Hash)
  module HashSet = Set.Make (Hash)

  module Level = struct
    type t = int [@@deriving jsoo]

    let compare = Int.compare
  end

  module LevelMap = Map.Make (Level)

  module Staker = struct
    type t = string [@@deriving jsoo]

    let compare = String.compare
  end

  module StakerSet = Set.Make (Staker)
  module StakerMap = Map.Make (Staker)
  module Id = String
  module IdMap = Map.Make (Id)

  module Round = struct
    type t = int [@@deriving jsoo]

    let compare = Int.compare
  end

  module RoundSet = Set.Make (Round)
  module RoundMap = Map.Make (Round)

  module LevelRound = struct
    type t = {
      level : int;
      round : int;
    }

    let compare lr1 lr2 =
      match compare lr1.level lr2.level with
      | 0 -> compare lr1.round lr2.round
      | i -> i
  end

  module LevelRoundMap = Map.Make (LevelRound)

  module SORU_DATA = struct
    module Commitment_tree = struct
      module type DataGetter = sig
        open RPC.Types

        val commitment_period : unit -> int

        (** [predecessors_storage_capacity] is an approximation of the "worst
            case scenario" of what would be needed to store the predecessor of
            each commitment of the tree.

            Note that the predecessors storage will be restored each time a new
            tree is built, the only time this storage is relevant is when you're
            working on a single commitment tree and you rely on the automatic
            update at each head fetched, the update will be way much smoother. *)
        val predecessors_storage_capacity : unit -> int

        val get_lcc :
          rollup_id:string -> (SORU.L1.commitment * SORU.L1.C.inbox_level) Lwt.t

        val get_commitments :
          rollup_id:string -> inbox_level:int -> SORU.L1.commitments Lwt.t

        val get_stakers_ids :
          rollup_id:string -> commitment:string -> SORU.L1.stakers_ids Lwt.t

        val get_staker_id :
          rollup_id:string -> staker_hash:string -> SORU.L1.staker_id Lwt.t

        val get_stakers : rollup_id:string -> SORU.L1.stakers Lwt.t

        val get_commitment_infos :
          rollup_id:string ->
          commitment:string ->
          SORU.L1.commitment_infos option Lwt.t
      end

      module Make (DG : DataGetter) = struct
        open RPC.Types

        type t = {
          commitment : SORU.L1.commitment;
          stakers : SORU.L1.stakers;
          childs : t list;
        }

        module Commitments_infos : sig
          val find_opt : SORU.L1.commitment -> SORU.L1.commitment_infos option

          val add : SORU.L1.commitment -> SORU.L1.commitment_infos -> unit

          val filter_clear :
            (SORU.L1.commitment -> SORU.L1.commitment_infos -> bool) -> unit

          val clear_on_change : rollup_id:string -> unit
        end = struct
          let rollup_id = ref ""

          let tbl : (SORU.L1.commitment, SORU.L1.commitment_infos) Hashtbl.t =
            Hashtbl.create @@ DG.predecessors_storage_capacity ()

          let find_opt commitment = Hashtbl.find_opt tbl commitment

          let add commitment commitment_info =
            Hashtbl.add tbl commitment commitment_info

          let filter_clear filter =
            let filter_map commitment commitment_info =
              if filter commitment commitment_info then
                Some commitment_info
              else
                None in
            Hashtbl.filter_map_inplace filter_map tbl

          let clear_on_change ~rollup_id:rollup_id' =
            if String.equal !rollup_id rollup_id' then begin
              Hashtbl.clear tbl ;
              rollup_id := rollup_id'
            end
        end

        let next_inbox_level inbox_level = inbox_level + DG.commitment_period ()

        module Maps = struct
          (** @staker_id_to_hash: staker's index to a staker's hash
              @inbox_lvl_to_cmts: inbox level to commitments
              @cmt_to_stakers_ids: commitment to stakers' indexes *)
          type t = {
            mutable staker_id_to_hash : SORU.L1.staker IdMap.t;
            mutable inbox_lvl_to_cmts : SORU.L1.C.commitments LevelMap.t;
            mutable cmt_to_stakers_ids : SORU.L1.stakers_ids HashMap.t;
          }

          let make () =
            {
              staker_id_to_hash = IdMap.empty;
              inbox_lvl_to_cmts = LevelMap.empty;
              cmt_to_stakers_ids = HashMap.empty;
            }

          let fill_staker_id_to_hash ~rollup_id ~stakers_hashes maps =
            let set staker_id staker_hash =
              maps.staker_id_to_hash <-
                IdMap.add staker_id staker_hash maps.staker_id_to_hash in
            Lwt_list.iter_s
              (fun staker_hash ->
                let%lwt staker_id = DG.get_staker_id ~rollup_id ~staker_hash in
                set staker_id staker_hash ;
                Lwt.return_unit)
              stakers_hashes

          let fill_cmt_to_stakers_ids ~rollup_id commitments maps =
            let set commitment stakers_ids =
              maps.cmt_to_stakers_ids <-
                HashMap.add commitment stakers_ids maps.cmt_to_stakers_ids in
            Lwt_list.iter_s
              (fun commitment ->
                try%lwt
                  let%lwt stakers_ids =
                    DG.get_stakers_ids ~rollup_id ~commitment in
                  set commitment stakers_ids ;
                  Lwt.return_unit
                with RPC.Error.Storage_error ->
                  set commitment [] ;
                  Lwt.return_unit)
              commitments

          let fill_inbox_lvl_to_cmt ~rollup_id ~start_inbox_level maps =
            let set inbox_level commitments =
              maps.inbox_lvl_to_cmts <-
                LevelMap.add inbox_level commitments maps.inbox_lvl_to_cmts
            in
            let rec fill inbox_level =
              let%lwt commitments_opt =
                DG.get_commitments ~rollup_id ~inbox_level in
              match commitments_opt with
              | None -> Lwt.return_unit
              | Some commitments ->
                set inbox_level commitments ;
                let%lwt () =
                  fill_cmt_to_stakers_ids ~rollup_id commitments maps in
                fill (next_inbox_level inbox_level) in
            fill start_inbox_level
        end

        let get_predecessor ~rollup_id ~commitment =
          match Commitments_infos.find_opt commitment with
          | None -> begin
            match%lwt DG.get_commitment_infos ~rollup_id ~commitment with
            | Some commitment_infos ->
              Commitments_infos.add commitment commitment_infos ;
              Lwt.return_some commitment_infos.SORU.L1.predecessor
            | None -> Lwt.return_none
          end
          | Some commitment_infos ->
            Lwt.return_some commitment_infos.SORU.L1.predecessor

        let make_tree ~rollup_id ~lcc_hash ~lcc_inbox_level maps =
          let rec make commitment inbox_level =
            let stakers =
              let stakers_ids =
                HashMap.find_opt commitment maps.Maps.cmt_to_stakers_ids in
              let stakers_ids = Option.value ~default:[] stakers_ids in
              List.filter_map
                (fun staker_id ->
                  IdMap.find_opt staker_id maps.Maps.staker_id_to_hash)
                stakers_ids in
            let child_inbox_level = next_inbox_level inbox_level in
            let childs_commitments =
              LevelMap.find_opt child_inbox_level maps.Maps.inbox_lvl_to_cmts
            in
            let%lwt childs =
              Lwt_list.filter_map_s
                (fun child_commitment ->
                  match%lwt
                    get_predecessor ~rollup_id ~commitment:child_commitment
                  with
                  | Some predecessor when predecessor = commitment ->
                    let%lwt child = make child_commitment child_inbox_level in
                    Lwt.return_some child
                  | _ -> Lwt.return_none)
                (Option.value ~default:[] childs_commitments) in
            Lwt.return { commitment; stakers; childs } in
          make lcc_hash lcc_inbox_level

        let build ~rollup_id =
          Commitments_infos.clear_on_change ~rollup_id ;
          let%lwt lcc_hash, lcc_inbox_level = DG.get_lcc ~rollup_id in
          let _clear_commitments_infos_below_lcc =
            Commitments_infos.filter_clear
            @@ fun _commitment SORU.L1.{ inbox_level; _ } ->
            lcc_inbox_level < inbox_level in
          let%lwt stakers_hashes = DG.get_stakers ~rollup_id in
          let maps = Maps.make () in
          let%lwt () =
            Maps.fill_staker_id_to_hash ~rollup_id ~stakers_hashes maps in
          let () (* we add [lcc_inbox_level] and [lcc_hash] at first *) =
            maps.Maps.inbox_lvl_to_cmts <-
              LevelMap.add lcc_inbox_level [] maps.Maps.inbox_lvl_to_cmts ;
            maps.Maps.cmt_to_stakers_ids <-
              HashMap.add lcc_hash [] maps.Maps.cmt_to_stakers_ids in
          let%lwt () =
            let start_inbox_level = next_inbox_level lcc_inbox_level in
            Maps.fill_inbox_lvl_to_cmt ~rollup_id ~start_inbox_level maps in
          let tree = make_tree ~rollup_id ~lcc_hash ~lcc_inbox_level maps in
          tree
      end
    end
  end
end
