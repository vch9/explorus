(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** - X : [base_round_displayed]
    - Y : [base_level_displayed]
    - arbitratry choices, these can be highered but it can become heavy at some
      point : X*Y = 64 cells displayed by default *)
let base_round_displayed = 8

let base_level_displayed = 8

(** The number of levels for which their consensus information is kept.
    Increasing this value may result in performance losses. *)
let consensus_tab_level_capacity = 3

(** The number of rounds displayed for the first old level. Increasing this
    value may result in performance losses. *)
let first_old_level_consensus_tabs_displayed = 5

(** The number of rounds displayed for the old levels. Increasing this value may
    result in performance losses. *)
let old_level_consensus_tabs_displayed = 1

let small_cache_size = 10 (* S *)

let medium_cache_size = 50 (* M *)

let large_cache_size = 100 (* L *)

let extra_large_cache_size = 500 (* XL *)

let upper_bound_block_delay = 30.

let upper_bound_duration =
  upper_bound_block_delay *. Float.of_int base_level_displayed

let endorsing_refresh_cyle_duration = 300. (* 5 minutes *)

let monitor_update_threshold = 2

let maximum_non_empty_blocks =
  50 (* used in explorer and minimize the number of non_empty_blocks *)

let threshold_explorer_operations = 500

let validators_rights_range_on_protocol_transition = 4

let genesis_level = 0
