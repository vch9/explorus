(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type infos = { addresses : string list } [@@deriving encoding]

type 'a networks = {
  mainnet : 'a;
  ghostnet : 'a option;
  mumbainet : 'a option;
  mondaynet : 'a option;
  dailynet : 'a option;
}
[@@deriving encoding { ignore }]

type parameters = infos networks [@@deriving encoding]

type protocol_transitions =
  (* int : id of the block where the next transition happens *)
  int networks
[@@deriving encoding]

type known_baker = {
  name : string;
  pkh : string;
  email : string option;
}
[@@deriving encoding]

type known_bakers = (known_baker list[@wrap "known_bakers"])
[@@deriving encoding]

type 'a components = {
  cp_baking_rights : 'a;
  cp_round_clock : 'a;
  cp_latest_blocks : 'a;
  cp_consensus_ops : 'a;
  cp_bakers_activity : 'a;
  cp_explorer : 'a;
  cp_soru : 'a;
  cp_info_bar : 'a;
  cp_consensus_progression : 'a;
  cp_network_constants : 'a;
  cp_share_url : 'a;
  cp_pyrometer : 'a;
  cp_protocol_injection : 'a;
  cp_settings : 'a;
}
[@@deriving encoding]

type components_states = bool components [@@deriving encoding]
