(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let verbose ~__FUNCTION__ =
  if Central_state.Debug.verbose () = 1 then Log.log __FUNCTION__

let default_metadata =
  {
    meta_baker = None;
    meta_proposer = None;
    meta_level_info = None;
    meta_balance_updates = None;
  }

let get_number_of_manager_operations { ops_monitor_ops; _ } =
  verbose ~__FUNCTION__ ;
  List.fold_left
    (fun n_ops op -> n_ops + List.length op.op_contents)
    0 ops_monitor_ops

let get_number_of_manager_operations_batches { ops_monitor_ops; _ } =
  verbose ~__FUNCTION__ ;
  List.length ops_monitor_ops

let get_baker_reward_bonus balance_updates =
  Option.perform balance_updates ~default:0.
    ~perform:
      (List.fold_left
         (fun rew balance_update ->
           match balance_update.bu_kind with
           | Minted Baking_bonuses -> Float.abs balance_update.bu_change
           | _ -> rew)
         0.)

(* The functions [get_total_endorsement_power] and [get_endorsers] below can
   (easily) be merged for efficiency reasons, but for reading purposes we'll
   leave it like this for now. *)
let get_total_endorsement_power { ops_endorsements; _ } =
  verbose ~__FUNCTION__ ;
  List.fold_left
    (fun total_e_p operation ->
      match List.nth_opt operation.op_contents 0 with
      (* endorsement power is only accessible from endorsement metadata, when it
         is not accessible the operation is ignored *)
      | Some
          (Endorsement
            {
              coc_metadata = Some { cocm_endorsement_power = Some power; _ };
              _;
            }) -> total_e_p + power
      | _ -> total_e_p)
    0 ops_endorsements

let get_endorsers { ops_endorsements; _ } =
  verbose ~__FUNCTION__ ;
  List.fold_left
    (fun endorsers op ->
      (* endorsements from delegates are only accessible from endorsement
         metadata, when they are not accessible the operations are ignored *)
      match List.nth_opt op.op_contents 0 with
      | Some
          (Endorsement
            { coc_metadata = Some { cocm_delegate = Some delegate; _ }; _ }) ->
        EndorserSet.add delegate endorsers
      | _ -> endorsers)
    EndorserSet.empty ops_endorsements

module Protocol_transition = struct
  exception Network_mismatch

  let transitions : Json_types.protocol_transitions ref =
    ref
      Json_types.
        {
          mainnet = 0;
          ghostnet = None;
          mumbainet = None;
          mondaynet = None;
          dailynet = None;
        }

  let transitions_setting_trigger = SafeTrigger.create ()

  let set_transitions protocol_transitions =
    if not @@ SafeTrigger.access transitions_setting_trigger then begin
      SafeTrigger.trigger transitions_setting_trigger ;
      transitions := protocol_transitions
    end

  let default_transition = (* to avoid crashes *) -1

  let rec get_transition ?network () =
    (* @todo : handling customized nets (i.e "private/sandboxed" networks)
       transitions could be made in a near future if we allow users to set their
       own protocol transition blocks from settings *)
    let open Central_state.Network in
    let open Json_types in
    let network = Option.value_delayed ~default:get_network_state network in
    let get_transition_opt transition_opt =
      match transition_opt with
      | None -> Lwt.fail Network_mismatch
      | Some transition_opt -> Lwt.return transition_opt in
    match network with
    | Mainnet -> Lwt.return !transitions.mainnet
    | Ghostnet -> get_transition_opt !transitions.ghostnet
    | Mumbainet -> get_transition_opt !transitions.mumbainet
    | Mondaynet -> get_transition_opt !transitions.mondaynet
    | Dailynet -> get_transition_opt !transitions.dailynet
    | Customnet -> begin
      try%lwt
        let%lwt { network_version = { chain_name }; _ } =
          Rpc.get_node_version () in
        let network = chain_name_to_network chain_name in
        get_transition ~network ()
      with Unknown_network -> Lwt.return default_transition
    end

  let set_protocol () =
    let%lwt block = Rpc.get_block () in
    let protocol = Protocol.identification block.bl_protocol in
    Central_state.Protocol.set_current protocol ;
    Lwt.return_unit
end

module Timestamped = struct
  type infos = {
    proposed : float;
    received : float;
  }

  type block_stamped = {
    block : block;
    ts_infos : infos;
  }
end

module TS = Timestamped

(** The purpose of [heads_listener] is to be chained with the heads stream
    listener. *)
let heads_listener : TS.block_stamped list Listener.t = Listener.create ()

let update_on_transitions level =
  if
    level = Konstant.genesis_level + 1
    || level = Central_state.Protocol.get_transition ()
  then (
    let%lwt network_constants = Rpc.get_network_constants () in
    Central_state.Network.set_network_constants network_constants ;
    Protocol_transition.set_protocol ()
  ) else
    Lwt.return_unit

let _monitor_incoming_heads =
  (* when HDStream.stream fails, it is re-executed and retrieves the current
     head already obtained by the initial HDStream.stream *)
  let last_head = ref None in
  let update_caches heads () =
    let%lwt heads =
      Lwt_list.filter_map_s
        (fun head ->
          let%lwt () = update_on_transitions head.mh_level in
          let hash = head.mh_hash in
          let received = head.mh_receipt_timestamp in
          let proposed = Lib_tz.parse_tezos_timestamp head.mh_timestamp in
          match !last_head with
          | Some last_head when last_head = hash -> Lwt.return_none
          | _ ->
            last_head := Some hash ;
            let%lwt block = Rpc.get_block ~bid:(Rpc.BId.Hash hash) () in
            Lwt.return_some TS.{ block; ts_infos = { proposed; received } })
        heads in
    Listener.trigger heads_listener heads ;
    Lwt.return_unit in
  ignore
  @@ Listener.connect Rpc.HDStream.listener
  @@ fun heads -> Lwt.async @@ update_caches heads

module C = struct
  (* capacity is set accordingly to its main use, i.e in 'latest_blocks' and
     'baking_rights', it could be more *)
  let capacity = Konstant.base_level_displayed
end

module BoundedLevelMap = BoundedMap.Make (LevelMap) (C)

module Blockchain = struct
  type timestamp = {
    time : string;
    date : string;
  }

  type missing_endorser = {
    miss_endorser : string;
    miss_slots : int;
  }

  type block_details = {
    bd_level : int;
    bd_round : int;
    bd_hash : string;
    bd_pred_hash : string;
    bd_manager_ops : int; (* transaction, origination... *)
    bd_manager_ops_batches : int;
    bd_baker : string;
    bd_baker_rew : float;
    bd_proposer : string;
    bd_proposer_rew : int;
    bd_endorsement_power : float;
    bd_missed_slots : int;
    bd_missing_endorsers : missing_endorser list;
    bd_timestamp : timestamp;
    bd_fitness : fitness;
  }

  type t = block_details HashMap.t BoundedLevelMap.t

  type chain_state = {
    chain : t;
    head_state : string;
  }

  let chain_state : chain_state ConcurrentValue.t =
    ConcurrentValue.create
    @@ {
         chain = BoundedLevelMap.empty;
         head_state = "BLockGenesisGenesisGenesisGenesisGenesis";
       }

  (** [recover ()] will recover current blockchain from actual state as a list *)
  let recover () =
    let { chain; head_state } = ConcurrentValue.get chain_state in
    (* @todo : this can probably be optimized *)
    let rev_chain = BoundedLevelMap.to_rev_seq chain in
    Lwt.return
    @@ (Seq.fold_left
          (fun (hash, l) (_level, hashmap) ->
            let block = HashMap.find hash hashmap in
            (block.bd_pred_hash, block :: l))
          (head_state, []) rev_chain
       |> snd |> List.rev)

  let get_missing_endorsers level endorsers =
    (* @todo : can be optimized *)
    let%lwt validators_pr =
      Cache.Validators.get level Rpc.get_validators
        (ConcurrentValue.get Central_state.Cache.validators) in
    let validators_slots =
      List.map
        (fun v -> (v.val_delegate, List.length v.val_slots))
        validators_pr in
    let validators, _ = List.split validators_slots in
    let missing_endorsers =
      EndorserSet.diff (EndorserSet.of_list validators) endorsers in
    Lwt.return
    @@ List.map
         (fun miss_endorser ->
           {
             miss_endorser;
             miss_slots =
               Option.value ~default:Pretty.none_int
               @@ List.assoc_opt miss_endorser validators_slots;
           })
         (EndorserSet.elements missing_endorsers)

  let get_one_slot_of_chain block =
    let network_constants = Central_state.Network.get_network_constants () in
    let metadata = Option.value ~default:default_metadata block.bl_metadata in
    let baker = metadata.meta_baker in
    let proposer = metadata.meta_proposer in
    let baker_rew =
      Lib_tz.baker_reward
      @@ get_baker_reward_bonus metadata.meta_balance_updates in
    let proposer_rew =
      Lib_tz.proposer_reward network_constants.nc_baking_reward_fixed_portion
    in
    let baker = Option.value ~default:Pretty.none_str baker in
    let proposer = Option.value ~default:Pretty.none_str proposer in
    let fitness = block.bl_header.hd_fitness in
    let LevelRound.{ level; round } = Rpc.Process.get_level_round block in
    let manager_ops = get_number_of_manager_operations block.bl_operations in
    let manager_ops_batches =
      get_number_of_manager_operations_batches block.bl_operations in
    let raw_endo_power = get_total_endorsement_power block.bl_operations in
    let percentage_endo_power =
      Compute.round_by_2
      @@ 100.
         *. Int.to_float raw_endo_power
         /. Int.to_float network_constants.nc_consensus_committee_size in
    let missed_slots =
      network_constants.nc_consensus_committee_size - raw_endo_power in
    let%lwt missing_endorsers =
      (* we handle genesis block *)
      if level = 0 then
        Lwt.return_nil
      else
        get_missing_endorsers (level - 1) @@ get_endorsers block.bl_operations
    in
    let time, date = Lib_tz.parse_tezos_time_date block.bl_header.hd_timestamp in
    Lwt.return
      {
        bd_level = level;
        bd_round = round;
        bd_hash = block.bl_hash;
        bd_pred_hash = block.bl_header.hd_predecessor;
        bd_manager_ops = manager_ops;
        bd_manager_ops_batches = manager_ops_batches;
        bd_baker = baker;
        bd_baker_rew = baker_rew;
        bd_proposer = proposer;
        bd_proposer_rew = Float.to_int proposer_rew;
        bd_endorsement_power = percentage_endo_power;
        bd_missed_slots = missed_slots;
        bd_missing_endorsers = missing_endorsers;
        bd_timestamp = { time; date };
        bd_fitness = fitness;
      }

  let update_state block =
    ConcurrentValue.update chain_state (fun chain_state ->
        Lwt.return { chain_state with head_state = block.bl_hash })

  let update_chain block hash level =
    ConcurrentValue.update chain_state (fun chain_state ->
        let update_one_slot () =
          let%lwt one_slot = get_one_slot_of_chain block in
          Lwt.return
          @@ {
               chain_state with
               chain =
                 BoundedLevelMap.update_with_default level
                   ~default:HashMap.empty
                   (HashMap.add hash one_slot)
                   chain_state.chain;
             } in
        match BoundedLevelMap.find_opt level chain_state.chain with
        | None -> update_one_slot ()
        | Some hashmap ->
          if HashMap.mem hash hashmap then
            Lwt.return chain_state
          else
            update_one_slot ())

  let fill_one_block block =
    let LevelRound.{ level; _ } = Rpc.Process.get_level_round block in
    let hash = block.bl_hash in
    update_chain block hash level

  let rec fill_all_chain_from block = function
    | level when level <= 0 -> Lwt.return_unit
    | level ->
      let searched_hash = block.bl_header.hd_predecessor in
      let%lwt block = Rpc.get_block ~bid:(Rpc.BId.Hash searched_hash) () in
      let%lwt () = fill_one_block block in
      fill_all_chain_from block (level - 1)

  let update_chain block =
    let%lwt () = update_state block in
    let%lwt () = fill_one_block block in
    let number_of_levels_available = block.bl_header.hd_level + 1 in
    let fill_size =
      min number_of_levels_available Konstant.base_level_displayed in
    fill_all_chain_from block fill_size

  let chain_update_listener : block_details list Listener.t = Listener.create ()

  let _update_chain =
    ignore
    @@ Listener.connect heads_listener
    @@ fun heads ->
    Lwt.async @@ fun () ->
    let%lwt () =
      Lwt_list.iter_s (fun TS.{ block; _ } -> update_chain block) heads in
    let%lwt chain = recover () in
    Listener.trigger chain_update_listener chain ;
    Lwt.return_unit
end

module HashTick = struct
  type t = {
    mutable hash : string;
    mutable tick : int;
  }

  let make ~hash ~tick = { hash; tick }

  let reset ~tick ~block_hash ~tick_diff =
    tick.hash <- block_hash ;
    tick.tick <- tick_diff

  let tick_diff ~timestamp =
    let current_timestamp = Time.universal () in
    let block_timestamp = Lib_tz.parse_tezos_timestamp timestamp in
    Float.to_int @@ (current_timestamp -. block_timestamp)

  let increment ~block_hash ~tick ~timestamp =
    if tick.hash = block_hash then
      tick.tick <- tick.tick + 1
    else
      let tick_diff = tick_diff ~timestamp in
      reset ~tick ~block_hash ~tick_diff
end

module Consensus = struct
  [@@@ocamlformat "wrap-comments=false"]
  (* The concept of Consensus is organized in such a way that we have:

     For all levels: (LevelInfo.t LevelMap.t)
     - Validator information (BakerInfo.t BakerMap.t)
     - For all rounds: (State.t RoundMap.t)
        - Information about the block, if it exists (BlockInfo.t)
        - Endorsement and preendorsement votes: (Votings.t)
            - Ballots (Ballots.t)
            - Baker operations (Operations.t BakerMap.t) *)

  module Operations = struct
    module Status = struct
      type t =
        | Applied
        | Outdated

      let merge status old_status =
        match (status, old_status) with
        | Outdated, Outdated -> Outdated
        | _ -> Applied
    end

    type op = {
      status : Status.t;
      timestamp : float;
      delta : float option;
    }

    type t = op HashMap.t

    let get_delta ~block_timestamp timestamp () =
      Option.map (( -. ) timestamp) block_timestamp

    let update_delta ~block_timestamp delta =
      let update_delta ({ timestamp; delta; _ } as op) =
        let delta =
          Option.bind_none delta
          @@ get_delta ~block_timestamp:(Some block_timestamp) timestamp in
        { op with delta } in
      HashMap.map update_delta delta

    let create ~block_timestamp ~hash ~status ~timestamp =
      let delta = get_delta ~block_timestamp timestamp () in
      HashMap.singleton hash { status; timestamp; delta }

    let merge_info info old_info =
      let status = Status.merge info.status old_info.status in
      let timestamp = old_info.timestamp in
      let delta =
        Option.union
          (fun _delta old_delta -> old_delta)
          info.delta old_info.delta in
      { status; timestamp; delta }

    let merge = HashMap.simple_union @@ fun _hash -> merge_info

    let at_least_one_has_been_applied operations =
      HashMap.exists
        (fun _hash { status; _ } -> status = Status.Applied)
        operations
  end

  module Ballots = struct
    type t = {
      counter : int;
      percent_progression : float;
      consensus : bool;
    }

    let empty = { counter = 0; percent_progression = 0.; consensus = false }

    let vote ~weight { counter; _ } =
      let { nc_consensus_committee_size; nc_consensus_threshold; _ } =
        Central_state.Network.get_network_constants () in
      let counter = counter + weight in
      let percent_progression =
        Lib_tz.votings_progression_percentage counter
          nc_consensus_committee_size in
      let consensus = Lib_tz.quorum_reached counter nc_consensus_threshold in
      { counter; percent_progression; consensus }
  end

  module Votings = struct
    type vote = {
      hash : string;
      status : Operations.Status.t;
      timestamp : float;
      kind : EndoPreendo.kind;
      delegate : validator;
    }

    type voting = {
      ballots : Ballots.t;
      bakers : Operations.t BakerMap.t;
      reaching_quorum_operation_timestamp : float option;
      reaching_quorum_operation_delta : float option;
      last_operation_timestamp : float option;
      last_operation_delta : float option;
    }

    type t = voting EndoPreendo.t

    let empty_info =
      {
        ballots = Ballots.empty;
        bakers = BakerMap.empty;
        reaching_quorum_operation_timestamp = None;
        reaching_quorum_operation_delta = None;
        last_operation_timestamp = None;
        last_operation_delta = None;
      }

    let empty = EndoPreendo.init empty_info

    let already_voted ~delegate:{ val_delegate; _ } ~bakers =
      Option.perform
        (BakerMap.find_opt val_delegate bakers)
        ~default:false ~perform:Operations.at_least_one_has_been_applied

    let update_ballots ~status ~delegate:({ val_slots; _ } as delegate) ~bakers
        ballots =
      if status = Operations.Status.Outdated || already_voted ~delegate ~bakers
      then
        ballots
      else
        let weight = List.length val_slots in
        Ballots.vote ~weight ballots

    let update_bakers ~block_timestamp ~hash ~status ~timestamp
        ~delegate:{ val_delegate; _ } bakers =
      let ops = Operations.create ~block_timestamp ~hash ~status ~timestamp in
      BakerMap.update_with_default val_delegate ~default:ops
        (Operations.merge ops) bakers

    let update_reaching_quorum_operation_timestamp ~timestamp
        ~ballots:Ballots.{ consensus; _ } = function
      | None when consensus -> Some timestamp
      | old_timestamp when consensus -> old_timestamp
      | _ -> None

    let update_last_operation_timestamp ~timestamp = function
      | None -> Some timestamp
      | Some old_timestamp -> Some (Float.max old_timestamp timestamp)

    let get_delta ~block_timestamp timestamp () =
      Option.merge ( -. ) timestamp block_timestamp

    let update_delta ~block_timestamp delta =
      let update_delta
          ({
             bakers;
             reaching_quorum_operation_timestamp;
             reaching_quorum_operation_delta;
             last_operation_timestamp;
             last_operation_delta;
             _;
           } as votings) =
        let bakers =
          BakerMap.map (Operations.update_delta ~block_timestamp) bakers in
        let reaching_quorum_operation_delta =
          Option.bind_none reaching_quorum_operation_delta
          @@ get_delta ~block_timestamp:(Some block_timestamp)
               reaching_quorum_operation_timestamp in
        let last_operation_delta =
          Option.bind_none last_operation_delta
          @@ get_delta ~block_timestamp:(Some block_timestamp)
               last_operation_timestamp in
        {
          votings with
          bakers;
          reaching_quorum_operation_delta;
          last_operation_delta;
        } in
      EndoPreendo.map update_delta delta

    let update_votes ~block_timestamp
        ~vote:{ hash; status; timestamp; kind; delegate } votings =
      let vote
          {
            ballots;
            bakers;
            reaching_quorum_operation_timestamp;
            last_operation_timestamp;
            _;
          } =
        let ballots = update_ballots ~status ~delegate ~bakers ballots in
        let bakers =
          update_bakers ~block_timestamp ~hash ~status ~timestamp ~delegate
            bakers in
        let reaching_quorum_operation_timestamp =
          update_reaching_quorum_operation_timestamp ~timestamp ~ballots
            reaching_quorum_operation_timestamp in
        let reaching_quorum_operation_delta =
          get_delta ~block_timestamp reaching_quorum_operation_timestamp ()
        in
        let last_operation_timestamp =
          update_last_operation_timestamp ~timestamp last_operation_timestamp
        in
        let last_operation_delta =
          get_delta ~block_timestamp last_operation_timestamp () in
        {
          ballots;
          bakers;
          reaching_quorum_operation_timestamp;
          reaching_quorum_operation_delta;
          last_operation_timestamp;
          last_operation_delta;
        } in
      EndoPreendo.map_with_kind ~kind vote votings
  end

  module BlockInfo = struct
    type t = {
      hash : string;
      timestamp : string;
      time : string;
      received : float;
      delta : float;
    }

    let get_timestamp { timestamp; _ } = Lib_tz.parse_tezos_timestamp timestamp

    let of_block TS.{ block; ts_infos } =
      let hash = block.bl_hash in
      let timestamp = block.bl_header.hd_timestamp in
      let received = ts_infos.TS.received in
      let time, _ = Lib_tz.parse_tezos_time_date timestamp in
      let delta = received -. Lib_tz.parse_tezos_timestamp timestamp in
      { hash; timestamp; time; received; delta }
  end

  module State = struct
    type t = {
      block_info : BlockInfo.t option;
      already_processed : bool;
      votings : Votings.t;
    }

    let empty =
      { block_info = None; already_processed = false; votings = Votings.empty }

    let update_block_info ~block_stamped block_info =
      Option.bind_none block_info @@ fun () ->
      Some (BlockInfo.of_block block_stamped)

    let update_already_processed ~already_processed has_been_processed =
      already_processed || has_been_processed

    let add_block_info ~block_stamped { block_info; already_processed; votings }
        =
      let votings =
        match block_info with
        | None ->
          Votings.update_delta
            ~block_timestamp:TS.(block_stamped.ts_infos.proposed)
            votings
        | _ -> votings in
      let block_info = update_block_info ~block_stamped block_info in
      let already_processed =
        update_already_processed ~already_processed:true already_processed in
      { block_info; already_processed; votings }

    let get_block_timestamp = Option.map BlockInfo.get_timestamp

    let add_operation ~vote { block_info; already_processed; votings } =
      let block_timestamp = get_block_timestamp block_info in
      let votings = Votings.update_votes ~block_timestamp ~vote votings in
      let already_processed =
        update_already_processed
          ~already_processed:(vote.Votings.status = Operations.Status.Applied)
          already_processed in
      { block_info; already_processed; votings }
  end

  module BakerInfo = struct
    type t = {
      endorsing_power : int;
      endorsing_power_percent : float;
      is_missing : bool;
    }

    let create ~delegate:{ val_slots; _ } ~is_missing =
      let { nc_consensus_committee_size; _ } =
        Central_state.Network.get_network_constants () in
      let endorsing_power = List.length val_slots in
      let endorsing_power_percent =
        Lib_tz.endorsing_power_percentage endorsing_power
          nc_consensus_committee_size in
      { endorsing_power; endorsing_power_percent; is_missing }

    let merge i1 i2 =
      {
        endorsing_power = i1.endorsing_power;
        endorsing_power_percent = i1.endorsing_power_percent;
        is_missing = i1.is_missing && i2.is_missing;
      }
  end

  module LevelInfo = struct
    type t = {
      bakers_info : BakerInfo.t BakerMap.t;
      block_already_proposed : bool;
      rounds : State.t RoundMap.t;
    }

    let empty =
      {
        bakers_info = BakerMap.empty;
        block_already_proposed = false;
        rounds = RoundMap.empty;
      }

    let update_baker_info ~delegate ~is_missing bakers_info =
      let baker_info = BakerInfo.create ~delegate ~is_missing in
      BakerMap.update_default delegate.val_delegate ~default:baker_info
        (BakerInfo.merge baker_info)
        bakers_info

    let fill_bakers_info ~block bakers_info =
      let%lwt is_missing =
        let previous_endorsers = get_endorsers block.bl_operations in
        let has_previous_endorsers =
          not @@ EndorserSet.is_empty previous_endorsers in
        let is_missing { val_delegate; _ } =
          has_previous_endorsers
          && not (EndorserSet.mem val_delegate previous_endorsers) in
        Lwt.return is_missing in
      let%lwt validators =
        Cache.Validators.get block.bl_header.hd_level Rpc.get_validators
          (ConcurrentValue.get Central_state.Cache.validators) in
      Lwt.return
      @@ List.fold_left
           (fun bakers_info delegate ->
             update_baker_info ~delegate ~is_missing:(is_missing delegate)
               bakers_info)
           bakers_info validators

    let update_rounds ~block_stamped rounds =
      let round = Rpc.Process.get_round block_stamped.TS.block in
      RoundMap.update_with_default round ~default:State.empty
        (State.add_block_info ~block_stamped)
        rounds

    let add_operation_to_rounds ~vote ~operation rounds =
      RoundMap.update_with_default operation.coc_round ~default:State.empty
        (State.add_operation ~vote)
        rounds

    let add_block_to_rounds ~block_stamped
        { bakers_info; block_already_proposed; rounds } =
      let%lwt bakers_info =
        if block_already_proposed then
          Lwt.return bakers_info
        else
          fill_bakers_info ~block:block_stamped.TS.block bakers_info in
      let rounds = update_rounds ~block_stamped rounds in
      let block_already_proposed = true in
      Lwt.return { bakers_info; block_already_proposed; rounds }

    let add_operation ~vote ~operation
        { bakers_info; block_already_proposed; rounds } =
      let bakers_info =
        update_baker_info ~delegate:vote.Votings.delegate ~is_missing:false
          bakers_info in
      let rounds = add_operation_to_rounds ~vote ~operation rounds in
      { bakers_info; block_already_proposed; rounds }
  end

  module LevelMap =
    BoundedMap.Make
      (LevelMap)
      (struct
        let capacity = Konstant.consensus_tab_level_capacity
      end)
end

module ConsensusStates = struct
  open Consensus

  type t = LevelInfo.t LevelMap.t

  let all_states : t ConcurrentValue.t = ConcurrentValue.create LevelMap.empty

  let high_priority_all_states = all_states

  let normal_priority_all_states =
    ConcurrentValue.unpriority high_priority_all_states

  let low_priority_all_states =
    ConcurrentValue.unpriority normal_priority_all_states

  (** The purpose of this listener is to be chained with the listener of the
      applied operation stream. It will be triggered each time applied
      operations have changed all_states. *)
  let applied_update_listener : t Listener.t = Listener.create ()

  (** The purpose of this listener is to be chained with the listener of the
      applied operation stream and outdated operation stream. It will be
      triggered each time operations have changed all_states. *)
  let update_listener : t Listener.t = Listener.create ()

  let update_state (block_stamped : TS.block_stamped) =
    ConcurrentValue.update high_priority_all_states
    @@ LevelMap.Lwt.update_with_default
         block_stamped.TS.block.bl_header.hd_level ~default:LevelInfo.empty
    @@ LevelInfo.add_block_to_rounds ~block_stamped

  let _heads_handling_connection =
    ignore
    @@ Listener.connect heads_listener
    @@ fun heads ->
    Lwt.async @@ fun () ->
    let%lwt () = Lwt_list.iter_s update_state heads in
    let all_states = ConcurrentValue.get all_states in
    Listener.trigger applied_update_listener all_states ;
    Listener.trigger update_listener all_states ;
    Lwt.return_unit

  let get_operation_validator { coc_slot; coc_level; _ } =
    let get_validators level =
      Cache.Validators.get level Rpc.get_validators
        (ConcurrentValue.get Central_state.Cache.validators) in
    ConcurrentValue.apply_with_update Central_state.Cache.validators_by_fst_slot
    @@ fun ~update validators_by_fst_slot ->
    let level = coc_level in
    let slot = coc_slot in
    let%lwt first_slots =
      Cache.ValidatorByFirstSlot.get ~level get_validators
        validators_by_fst_slot in
    let () =
      let updated_cache =
        Cache.ValidatorByFirstSlot.update ~level first_slots
          validators_by_fst_slot in
      update updated_cache in
    Lwt.return @@ Cache.ValidatorByFirstSlot.find_validator ~slot first_slots

  let add_operation ~timestamp ~status ~hash ~kind operation =
    let all_states =
      match status with
      | Operations.Status.Applied -> normal_priority_all_states
      | Operations.Status.Outdated -> low_priority_all_states in
    ConcurrentValue.apply_with_update all_states @@ fun ~update all_states ->
    match%lwt get_operation_validator operation with
    | None -> Lwt.return_false
    | Some delegate ->
      let vote = Votings.{ hash; status; timestamp; kind; delegate } in
      let all_states =
        LevelMap.update_with_default operation.coc_level
          ~default:LevelInfo.empty
          (LevelInfo.add_operation ~vote ~operation)
          all_states in
      update all_states ;
      Lwt.return_true

  let is_level_out_of_reach level =
    let perform (current_level, _) =
      (* Operations that will not be added to arrays are ignored *)
      current_level <= level + Konstant.consensus_tab_level_capacity in
    let max_level_binding =
      LevelMap.max_binding_opt (ConcurrentValue.get all_states) in
    Option.perform max_level_binding ~default:false ~perform

  let can_process_mo_content ~status content =
    match status with
    | Operations.Status.Applied -> true
    | Operations.Status.Outdated -> is_level_out_of_reach content.coc_level

  let process_mo_content ~timestamp ~status ~hash ~kind content =
    if can_process_mo_content ~status content then
      add_operation ~timestamp ~status ~hash ~kind content
    else
      Lwt.return_false

  let consensus_ops_iter ~parallelize f monitor_operations =
    let iter =
      if parallelize then
        Lwt_list.iter_p
      else
        Lwt_list.iter_s in
    iter
      (fun { mo_contents; mo_hash = hash; _ } ->
        iter
          (function
            | Preendorsement content -> f ~hash ~kind:`Preendorsement content
            | Endorsement content -> f ~hash ~kind:`Endorsement content
            | _ -> Lwt.return_unit)
          mo_contents)
      monitor_operations

  let consensus_ops_iter_p f monitor_operations =
    consensus_ops_iter ~parallelize:true f monitor_operations

  let consensus_ops_fold_left f acc monitor_operations =
    Lwt_list.fold_left_s
      (fun acc { mo_contents; mo_hash = hash; _ } ->
        Lwt_list.fold_left_s
          (fun acc -> function
            | Preendorsement content ->
              f acc ~hash ~kind:`Preendorsement content
            | Endorsement content -> f acc ~hash ~kind:`Endorsement content
            | _ -> Lwt.return acc)
          acc mo_contents)
      acc monitor_operations

  let handle_consensus_operations ~mo_listener ~status =
    ignore
    @@ Listener.connect mo_listener
    @@ fun monitor_operations ->
    let timestamp = Time.universal () in
    Lwt.async @@ fun () ->
    let%lwt has_been_updated =
      consensus_ops_fold_left
        (fun has_been_updated ~hash ~kind mo_content ->
          let%lwt updated =
            process_mo_content ~timestamp ~status ~hash ~kind mo_content in
          Lwt.return (has_been_updated || updated))
        false monitor_operations in
    if has_been_updated then begin
      let all_states = ConcurrentValue.get all_states in
      if status = Operations.Status.Applied then
        Listener.trigger applied_update_listener all_states ;
      Listener.trigger update_listener all_states
    end ;
    Lwt.return_unit

  let _applied_consensus_operations_handling_connection =
    handle_consensus_operations ~mo_listener:Rpc.MOStream.Applied.listener
      ~status:Operations.Status.Applied

  let _outdated_consensus_operations_handling_connection =
    handle_consensus_operations ~mo_listener:Rpc.MOStream.Outdated.listener
      ~status:Operations.Status.Outdated
end

module Clock : sig
  type round_info = {
    curr_round : int;
    curr_max_round : int;
    curr_pos_round : int;
  }

  val get_round_info : unit -> round_info

  val initialize : unit -> unit Lwt.t

  val run : protocol_transition_level:int -> unit -> unit
end = struct
  (* - a clock by definition on explorus and on tezos is what is able to compute
     the time that is flowing during tezos' rounds

     - a clock is extremely dependant from the protocol since it relies on the
     constants defined by the network, especially the [minimal_block_delay] and
     the [delay_increment_per_round] *)

  type effective_tick = {
    tick : HashTick.t;
    mutable minimal_block_delay : int;
    mutable delay_increment_per_round : int;
  }

  type round_info = {
    curr_round : int;
    curr_max_round : int;
    curr_pos_round : int;
  }

  type t = {
    round_info : round_info;
    effective_tick : effective_tick option;
  }

  let clock =
    ConcurrentValue.create
      {
        round_info = { curr_round = 0; curr_max_round = 0; curr_pos_round = 0 };
        effective_tick = None;
      }

  let initialize () =
    let { nc_minimal_block_delay; _ } =
      Central_state.Network.get_network_constants () in
    ConcurrentValue.simple_update clock @@ fun clock ->
    {
      clock with
      round_info =
        { clock.round_info with curr_max_round = nc_minimal_block_delay };
    }

  let update_on_transitions ~protocol_transition_level ~level clock =
    if level = Konstant.genesis_level + 1 || level = protocol_transition_level
    then
      let { nc_minimal_block_delay; nc_delay_increment_per_round; _ } =
        Central_state.Network.get_network_constants () in
      Option.iter
        (fun effective_tick ->
          effective_tick.delay_increment_per_round <-
            nc_delay_increment_per_round ;
          effective_tick.minimal_block_delay <- nc_minimal_block_delay)
        clock.effective_tick

  let get_round_info () = (ConcurrentValue.get clock).round_info

  let get_effective_tick block =
    let network_constants = Central_state.Network.get_network_constants () in
    let minimal_block_delay = network_constants.nc_minimal_block_delay in
    let delay_increment_per_round =
      network_constants.nc_delay_increment_per_round in
    let tick = HashTick.tick_diff ~timestamp:block.bl_header.hd_timestamp in
    {
      tick = HashTick.make ~hash:block.bl_hash ~tick;
      minimal_block_delay;
      delay_increment_per_round;
    }

  let compute_round_overflow ~delay_increment_per_round ~curr_round
      ~curr_max_round ~curr_pos_round =
    let rec compute_round_overflow_aux curr_round curr_max_round curr_pos_round
        =
      if curr_pos_round > curr_max_round then
        let curr_round = curr_round + 1 in
        let curr_pos_round = curr_pos_round - curr_max_round in
        let curr_max_round = curr_max_round + delay_increment_per_round in
        compute_round_overflow_aux curr_round curr_max_round curr_pos_round
      else
        (curr_round, curr_max_round, curr_pos_round) in
    compute_round_overflow_aux curr_round curr_max_round curr_pos_round

  let update_round_progression ~effective_tick curr_round =
    let { tick; minimal_block_delay; delay_increment_per_round } =
      effective_tick in
    let curr_pos_round = tick.HashTick.tick in
    let curr_max_round =
      minimal_block_delay + (curr_round * delay_increment_per_round) in
    let curr_round, curr_max_round, curr_pos_round =
      compute_round_overflow ~delay_increment_per_round ~curr_round
        ~curr_max_round ~curr_pos_round in
    { curr_round; curr_max_round; curr_pos_round }

  let update_round ~block ~current_round ~effective_tick =
    let block_hash = block.bl_hash in
    let tick = effective_tick.tick in
    let timestamp = block.bl_header.hd_timestamp in
    HashTick.increment ~block_hash ~tick ~timestamp ;
    update_round_progression ~effective_tick current_round

  let callback ~protocol_transition_level () =
    Lwt.async @@ fun () ->
    let%lwt head = Rpc.get_block () in
    let Tezos.Entities.LevelRound.{ round; level } =
      Rpc.Process.get_level_round head in
    ConcurrentValue.update clock @@ fun clock ->
    update_on_transitions ~protocol_transition_level ~level clock ;
    let effective_tick =
      Option.value_delayed
        ~default:(fun () -> get_effective_tick head)
        clock.effective_tick in
    let round_info =
      if clock.round_info.curr_max_round = 0 then
        clock.round_info
      else
        update_round ~block:head ~current_round:round ~effective_tick in
    Lwt.return { clock with round_info }

  let clock_trigger = SafeTrigger.create ()

  let run ~protocol_transition_level () =
    if not @@ SafeTrigger.access clock_trigger then begin
      SafeTrigger.trigger clock_trigger ;
      Lwt.async @@ fun () ->
      let%lwt head = Rpc.get_block () in
      let effective_tick = get_effective_tick head in
      let%lwt () =
        ConcurrentValue.update clock @@ fun clock ->
        Lwt.return { clock with effective_tick = Some effective_tick } in
      let open Central_state.Intervals in
      let callback = callback ~protocol_transition_level in
      add_intervals ~clearable_type:Unclearable ~callback ~loop_frequency:1. ;
      Lwt.return_unit
    end
end

module SORU = struct
  module L1 = struct
    module L1 = SORU.L1
    module L1_Rpc = Rpc.SORU.L1

    type staker_info = {
      last_commitment : L1.commitment_infos_with_hash option;
      id : L1.staker_id;
    }

    type stakers_info = staker_info StakerMap.t

    module StakerKey = struct
      type t = L1.staker * L1.staker_id

      let compare (hash1, id1) (hash2, id2) =
        Compute.lexico_compare [Staker.compare hash1 hash2; Id.compare id1 id2]
    end

    module Conflict = struct
      type t = {
        inbox_level : int;
        staker1 : StakerKey.t;
        staker2 : StakerKey.t;
      }

      let compare c1 c2 =
        Compute.lexico_compare
          [
            Int.compare c1.inbox_level c2.inbox_level;
            StakerKey.compare c1.staker1 c2.staker1;
            StakerKey.compare c1.staker2 c2.staker2;
          ]
    end

    module ConflictSet = Set.Make (Conflict)

    module Game = struct
      module Id = struct
        type t = {
          inbox_level : int;
          start_level : int;
          alice : L1.staker;
          bob : L1.staker;
        }

        let compare g1 g2 =
          Compute.lexico_compare
            [
              Int.compare g1.inbox_level g2.inbox_level;
              Int.compare g1.start_level g2.start_level;
              String.compare g1.alice g2.alice;
              String.compare g1.bob g2.bob;
            ]
      end

      module IdMap = Map.Make (Id)

      module Turn = struct
        type t = {
          turn : L1.player;
          game_state : L1.game_state;
          timeout : L1.timeout;
          last_agree_tick : int option;
        }
      end

      type ending = {
        final_level : int;
        refutation : L1.refutation option;
        game_result : L1.game_result;
      }

      type t = {
        conflict : Conflict.t;
        turns : Turn.t LevelMap.t;
        last_timeout : L1.timeout;
        ending : ending option;
      }
    end

    type games = Game.t Game.IdMap.t

    module Commitment_tree_DG = struct
      let commitment_period () =
        (* @todo: remove the line below.

           Currently named [sc_rollup_commitment_period_in_blocks] on mainnet,
           thus this ugly hack: *)
        let { nc_smart_rollup_commitment_period_in_blocks; _ } =
          Central_state.Network.get_network_constants () in
        Option.value ~default:30 nc_smart_rollup_commitment_period_in_blocks

      let predecessors_storage_capacity () =
        (* @todo: remove the lines below.

           Currently named [sc_rollup_commitment_period_in_blocks] and
           [sc_rollup_challenge_window_in_blocks] on mainnet, thus this ugly
           hack: *)
        let {
          nc_smart_rollup_commitment_period_in_blocks;
          nc_smart_rollup_challenge_window_in_blocks;
          _;
        } =
          Central_state.Network.get_network_constants () in
        let smart_rollup_commitment_period_in_blocks =
          Option.value ~default:30 nc_smart_rollup_commitment_period_in_blocks
        in
        let smart_rollup_challenge_window_in_blocks =
          Option.value ~default:80640 nc_smart_rollup_challenge_window_in_blocks
        in
        (* it does not need to be that accurate, we just need a good
           approximation *)
        smart_rollup_challenge_window_in_blocks
        / smart_rollup_commitment_period_in_blocks

      let get_lcc ~rollup_id =
        let%lwt L1.{ lp_hash; lp_level } =
          L1_Rpc.get_last_cemented_commitment_hash_with_level rollup_id in
        Lwt.return (lp_hash, lp_level)

      let get_commitments ~rollup_id ~inbox_level =
        L1_Rpc.get_commitments rollup_id inbox_level

      let get_stakers_ids ~rollup_id ~commitment =
        L1_Rpc.get_stakers_ids rollup_id commitment

      let get_staker_id ~rollup_id ~staker_hash =
        L1_Rpc.get_staker_id rollup_id staker_hash

      let get_stakers ~rollup_id = L1_Rpc.get_stakers rollup_id

      let get_commitment_infos ~rollup_id ~commitment =
        try%lwt
          let%lwt commitment_infos =
            L1_Rpc.get_commitment_infos rollup_id commitment in
          Lwt.return_some commitment_infos
        with Tezos.RPC.Error.Smart_rollup_unknown_commitment ->
          Lwt.return_none
    end

    module Commitment_tree = SORU_DATA.Commitment_tree.Make (Commitment_tree_DG)

    module type SMART_ROLLUP_STATIC = sig
      val smart_rollup_id : string

      val genesis_info : L1.genesis_info

      val kind : string
    end

    module type SMART_ROLLUP = sig
      include SMART_ROLLUP_STATIC

      val lcc_listener : L1.commitment_infos_with_hash Listener.t

      val stakers_info_listener : stakers_info Listener.t

      val conflicts_listener : ConflictSet.t Listener.t

      val get_commitment_tree : unit -> Commitment_tree.t Lwt.t

      val get_commitment_infos :
        L1.commitment -> L1.commitment_infos option Lwt.t

      val games_listener : games Listener.t

      val at_close : unit PendingFunctions.t
    end

    module Make (Smart_rollup_static : SMART_ROLLUP_STATIC) : SMART_ROLLUP =
    struct
      include Smart_rollup_static

      let at_close : unit PendingFunctions.t = PendingFunctions.create ()

      module Lcc = struct
        let get () =
          let%lwt L1.{ lp_hash; _ } =
            L1_Rpc.get_last_cemented_commitment_hash_with_level smart_rollup_id
          in
          try%lwt
            let%lwt cih_info =
              L1_Rpc.get_commitment_infos smart_rollup_id lp_hash in
            Lwt.return_some L1.{ cih_hash = lp_hash; cih_info }
          with Tezos.RPC.Error.Smart_rollup_unknown_commitment ->
            Lwt.return_none
      end

      module StakersInfo = struct
        let get_staker_info (staker : L1.staker) : staker_info Lwt.t =
          let%lwt last_commitment =
            L1_Rpc.get_staked_on_commitment smart_rollup_id staker in
          let%lwt id = L1_Rpc.get_staker_id smart_rollup_id staker in
          Lwt.return { last_commitment; id }

        let get (stakers : StakerSet.t) : stakers_info Lwt.t =
          let add_staker_info staker stakers_info =
            let%lwt staker_info = get_staker_info staker in
            let stakers_info = StakerMap.add staker staker_info stakers_info in
            Lwt.return stakers_info in
          StakerSet.Lwt.fold_s add_staker_info stakers StakerMap.empty

        let get_staker_id (stakers_info : stakers_info) (staker : L1.staker) :
            L1.staker_id option =
          Option.map
            (fun { id; _ } -> id)
            (StakerMap.find_opt staker stakers_info)
      end

      module Conflicts = struct
        let get_conflict ~(get_staker_id : L1.staker -> L1.staker_id option)
            ~(staker_key : StakerKey.t) : Conflict.t list Lwt.t =
          let staker, _id = staker_key in
          let make L1.{ c_other; c_our_commitment; _ } =
            Option.bind (get_staker_id c_other) @@ fun other_id ->
            let other_key = (c_other, other_id) in
            let staker1, staker2 =
              Compute.ordered StakerKey.compare staker_key other_key in
            let inbox_level = c_our_commitment.SORU.L1.inbox_level in
            Some Conflict.{ inbox_level; staker1; staker2 } in
          let%lwt conflicts = L1_Rpc.get_conflicts smart_rollup_id staker in
          let conflicts = List.filter_map make conflicts in
          Lwt.return conflicts

        let get ~(get_staker_id : L1.staker -> L1.staker_id option)
            ~(stakers_info : stakers_info) : ConflictSet.t Lwt.t =
          let add_conflict conflicts conflict =
            ConflictSet.add conflict conflicts in
          let add_staker_conflicts staker { id; _ } conflicts =
            let staker_key = (staker, id) in
            let%lwt staker_conflicts = get_conflict ~get_staker_id ~staker_key in
            let conflicts =
              List.fold_left add_conflict conflicts staker_conflicts in
            Lwt.return conflicts in
          StakerMap.Lwt.fold_s add_staker_conflicts stakers_info
            ConflictSet.empty
      end

      module Game = struct
        include Game

        module Id = struct
          include Id

          let from_refutation_game L1.{ rg_game; rg_alice; rg_bob } =
            {
              inbox_level = rg_game.L1.g_inbox_level;
              start_level = rg_game.L1.g_start_level;
              alice = rg_alice;
              bob = rg_bob;
            }

          let belongs_to (staker1 : L1.staker) (staker2 : L1.staker)
              { alice; bob; _ } =
            let staker1, staker2 =
              Compute.ordered Staker.compare staker1 staker2 in
            let alice, bob = Compute.ordered Staker.compare alice bob in
            Staker.compare staker1 alice = 0 && Staker.compare staker2 bob = 0
        end

        module Turn = struct
          include Turn

          let from_refutation_game_and_timeout ?last_agree_tick
              L1.{ rg_game; _ } timeout =
            let turn = rg_game.L1.g_turn in
            let game_state = rg_game.L1.g_game_state in
            { turn; game_state; timeout; last_agree_tick }

          let get_first_tick { game_state; _ } =
            let open L1 in
            match game_state with
            | Dissecting { gd_dissection; _ } ->
              Option.bind (List.hd_opt gd_dissection)
              @@ fun agreed_start_chunk -> Some agreed_start_chunk.gdc_tick
            | Final_move { gfm_agreed_start_chunk; _ } ->
              Some gfm_agreed_start_chunk.gdc_tick

          let add_last_agree_tick ~turn previous_turn =
            let last_agree_tick = get_first_tick turn in
            { previous_turn with last_agree_tick }
        end

        let make ~level ~game_id:Id.{ alice; bob; _ } ~refutation_game conflict
            =
          let bid = Rpc.BlockId.Level level in
          let%lwt timeout = L1_Rpc.get_timeout ~bid smart_rollup_id alice bob in
          match timeout with
          | None -> Lwt.return_none
          | Some timeout ->
            let turn =
              Turn.from_refutation_game_and_timeout refutation_game timeout
            in
            let turns = LevelMap.singleton level turn in
            let last_timeout = timeout in
            let ending = None in
            Lwt.return_some { conflict; turns; last_timeout; ending }

        let update_previous_turn_last_agree_tick ~level ~turn turns =
          match LevelMap.find_last_opt (( > ) level) turns with
          | None -> turns
          | Some (previous_level, previous_turn) ->
            let previous_turn = Turn.add_last_agree_tick ~turn previous_turn in
            LevelMap.add previous_level previous_turn turns

        let update_turn ~level ~game_id:Id.{ alice; bob; _ } ~refutation_game
            game =
          let bid = Rpc.BlockId.Level level in
          let%lwt timeout = L1_Rpc.get_timeout ~bid smart_rollup_id alice bob in
          match timeout with
          | None -> Lwt.return game
          | Some timeout when timeout.L1.t_last_turn_level < level ->
            Lwt.return { game with last_timeout = timeout }
          | Some timeout ->
            let turn =
              Turn.from_refutation_game_and_timeout refutation_game timeout
            in
            let turns =
              update_previous_turn_last_agree_tick ~level ~turn game.turns in
            let turns = LevelMap.add level turn turns in
            let last_timeout = timeout in
            Lwt.return { game with turns; last_timeout }

        let end_game ~level ~refutation ~game_result game =
          match game.ending with
          | Some _ -> game
          | None ->
            let turns =
              let open L1 in
              match (refutation, LevelMap.max_binding_opt game.turns) with
              | Some (Start _), _ | None, _ | _, None -> game.turns
              | Some (Move { rm_choice; _ }), Some (last_level, last_turn) ->
                let last_agree_tick = Some rm_choice in
                let last_turn = Turn.{ last_turn with last_agree_tick } in
                LevelMap.add last_level last_turn game.turns in
            let final_level = level in
            let ending = Some { final_level; refutation; game_result } in
            { game with turns; ending }
      end

      module Games = struct
        let empty : games = Game.IdMap.empty

        let is_conflict_matching ~game_id:Game.Id.{ inbox_level; alice; bob; _ }
            Conflict.
              {
                inbox_level = conflict_inbox_level;
                staker1 = staker1, _id1;
                staker2 = staker2, _id2;
              } =
          let alice, bob = Compute.ordered Staker.compare alice bob in
          let staker1, staker2 =
            Compute.ordered Staker.compare staker1 staker2 in
          inbox_level = conflict_inbox_level
          && String.equal alice staker1 && String.equal bob staker2

        let find_conflict_matching ~game_id conflicts =
          ConflictSet.find_first_opt (is_conflict_matching ~game_id) conflicts

        let rec add_game ~conflicts ~level ~staker games refutation_game =
          let game_id = Game.Id.from_refutation_game refutation_game in
          let update_game = function
            | None -> begin
              match find_conflict_matching ~game_id conflicts with
              | None -> Lwt.return_none
              | Some conflict ->
                Game.make ~level ~game_id ~refutation_game conflict
            end
            | Some game ->
              let%lwt game =
                Game.update_turn ~level ~game_id ~refutation_game game in
              Lwt.return_some game in
          let add_game games = Game.IdMap.Lwt.update game_id update_game games in
          if
            Game.IdMap.mem game_id games || level <= game_id.Game.Id.start_level
          then
            add_game games
          else
            (* Retreive previous turn of the game when it has already started *)
            let%lwt games =
              add_staker_games ~conflicts ~level:(level - 1) staker games in
            add_game games

        and add_staker_games ~conflicts ~level staker games =
          let bid = Rpc.BlockId.Level level in
          let%lwt staker_games =
            L1_Rpc.get_ongoing_refutation_games ~bid smart_rollup_id staker
          in
          Lwt_list.fold_left_s
            (add_game ~conflicts ~level ~staker)
            games staker_games

        let update ~conflicts ~level ~stakers games =
          StakerSet.Lwt.fold_s
            (add_staker_games ~conflicts ~level)
            stakers games

        let end_game ~level ~refutation ~game_result ~staker1 ~staker2 games =
          match
            Game.IdMap.find_first_opt (Game.Id.belongs_to staker1 staker2) games
          with
          | None -> games
          | Some (game_id, game) ->
            let game = Game.end_game ~level ~refutation ~game_result game in
            Game.IdMap.add game_id game games

        let end_games { bl_operations; bl_header; _ } (games : games) : games =
          let level = bl_header.hd_level in
          let update_with_refutation staker1 staker2 refutation metadata games =
            let open L1 in
            match metadata.operation_result with
            | Applied { game_status = Ended game_result } -> begin
              end_game ~level ~refutation ~game_result ~staker1 ~staker2 games
            end
            | _ -> games in
          let update_with_operations games = function
            | Smart_rollup_refute
                {
                  srr_rollup;
                  srr_manager = { man_source };
                  srr_opponent;
                  srr_refutation;
                  srr_metadata = Some metadata;
                }
              when String.equal srr_rollup smart_rollup_id ->
              update_with_refutation man_source srr_opponent
                (Some srr_refutation) metadata games
            | Smart_rollup_timeout
                {
                  srt_rollup;
                  srt_stakers = L1.{ rgp_alice; rgp_bob };
                  srt_metadata = Some metadata;
                  _;
                }
              when String.equal srt_rollup smart_rollup_id ->
              update_with_refutation rgp_alice rgp_bob None metadata games
            | _ -> games in
          let update_with_monitor_ops games { op_contents; _ } =
            List.fold_left update_with_operations games op_contents in
          List.fold_left update_with_monitor_ops games
            bl_operations.ops_monitor_ops
      end

      let get_commitment_tree () =
        Commitment_tree.build ~rollup_id:smart_rollup_id

      module Commitments_info = struct
        type t = L1.commitment_infos HashMap.t

        let force_get commitment =
          try%lwt
            let%lwt commitment_infos =
              L1_Rpc.get_commitment_infos smart_rollup_id commitment in
            Lwt.return_some commitment_infos
          with Tezos.RPC.Error.Smart_rollup_unknown_commitment ->
            Lwt.return_none

        let update commitment commitments_info =
          match HashMap.find_opt commitment commitments_info with
          | Some info -> Lwt.return (Some info, commitments_info)
          | None -> begin
            let%lwt info = force_get commitment in
            let add info = HashMap.add commitment info commitments_info in
            let commitments_info =
              Option.perform info ~default:commitments_info ~perform:add in
            Lwt.return (info, commitments_info)
          end
      end

      let commitments_info : Commitments_info.t ConcurrentValue.t =
        ConcurrentValue.create HashMap.empty

      let get_commitment_infos commitment =
        ConcurrentValue.apply_with_update commitments_info
        @@ fun ~update commitments_info ->
        let%lwt commitment_infos, commitments_info =
          Commitments_info.update commitment commitments_info in
        update commitments_info ;
        Lwt.return commitment_infos

      let lcc_listener : L1.commitment_infos_with_hash Listener.t =
        Listener.create ()

      let stakers_info_listener : stakers_info Listener.t = Listener.create ()

      let conflicts_listener : ConflictSet.t Listener.t = Listener.create ()

      let games : games ConcurrentValue.t = ConcurrentValue.create Games.empty

      let games_listener : games Listener.t = Listener.create ()

      let _daemon =
        let update TS.{ block; _ } =
          let level = block.bl_header.hd_level in
          let%lwt lcc = Lcc.get () in
          Option.iter (Listener.trigger lcc_listener) lcc ;
          let%lwt stakers = L1_Rpc.get_stakers smart_rollup_id in
          let stakers = StakerSet.of_list stakers in
          let%lwt stakers_info = StakersInfo.get stakers in
          Listener.trigger stakers_info_listener stakers_info ;
          let get_staker_id = StakersInfo.get_staker_id stakers_info in
          let%lwt conflicts = Conflicts.get ~get_staker_id ~stakers_info in
          Listener.trigger conflicts_listener conflicts ;
          let%lwt _update_games =
            ConcurrentValue.update games @@ fun games ->
            let%lwt games = Games.update ~conflicts ~level ~stakers games in
            let games = Games.end_games block games in
            Listener.trigger games_listener games ;
            Lwt.return games in
          Lwt.return_unit in
        let connection =
          Listener.connect heads_listener @@ fun heads ->
          Lwt.async @@ fun () -> Lwt_list.iter_s update heads in
        PendingFunctions.add at_close (fun () -> Listener.disconnect connection)
    end

    type t = (module SMART_ROLLUP)

    let make ~smart_rollup_id =
      let%lwt genesis_info = L1_Rpc.get_genesis_info smart_rollup_id in
      let%lwt kind = L1_Rpc.get_kind smart_rollup_id in
      let module Smart_rollup_static = struct
        let smart_rollup_id = smart_rollup_id

        let genesis_info = genesis_info

        let kind = kind
      end in
      let module Smart_rollup = Make (Smart_rollup_static) in
      Lwt.return (module Smart_rollup : SMART_ROLLUP)

    let current_smart_rollup : t option ConcurrentValue.t =
      ConcurrentValue.create None

    let stop (module Smart_rollup : SMART_ROLLUP) =
      PendingFunctions.run Smart_rollup.at_close ()

    let remove () =
      ConcurrentValue.update current_smart_rollup @@ fun smart_rollup ->
      Option.iter stop smart_rollup ;
      Lwt.return_none

    let set ~smart_rollup_id =
      ConcurrentValue.apply_with_update current_smart_rollup
      @@ fun ~update smart_rollup ->
      Option.iter stop smart_rollup ;
      let%lwt smart_rollup = make ~smart_rollup_id in
      update (Some smart_rollup) ;
      Lwt.return smart_rollup
  end
end
