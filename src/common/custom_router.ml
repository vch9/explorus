(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml

let network_param = "network"

let construct_network_param network = "?" ^ network_param ^ "=" ^ network

let set_title path =
  path |> String.split_on_char '/' |> List.rev |> Lib.List.hd_opt
  |> Option.iter @@ fun title ->
     let title =
       String.split_on_char '_' title
       |> List.map String.capitalize_ascii
       |> String.concat " " in
     let title =
       match title with
       | "" -> ""
       | title -> title ^ " " in
     let title =
       Printf.sprintf "Explorus %s- A Tezos Consensus Viewer by Functori" title
     in
     Dom_html.document##.title := Js.string title

let set_path ?(overwrite = false) ?(change_title = true) path =
  if change_title then set_title path ;
  if overwrite then
    Dom_html.window##.location##.href := Js.string path
  else
    (* we are forcing to change only the path while staying on the website *)
    Dom_html.window##.history##pushState
      (Js.string "") (Js.string "Explorus")
      (Js.some @@ Js.string @@ "/" ^ path)

let extract_network_query q = Uri.(get_query_param (of_string q) network_param)

let get_path () = Js.to_string Dom_html.window##.location##.pathname

let get_query () = Js.to_string Dom_html.window##.location##.search

let get_url () = Js.to_string Dom_html.window##.location##.href

let get_clean_path () =
  let path = get_path () in
  let length = String.length path in
  (* we ignore the first '/' char *)
  String.sub path 1 (length - 1)

let load_changing_path_methods refresh add_method paths =
  let fun_to_add path app =
    let current_lcs = get_clean_path () |> Page.of_path |> Page.to_lcs in
    let transition_lcs = path |> Page.of_path |> Page.to_lcs in
    let components_to_update =
      Page.LCS.transition current_lcs transition_lcs |> Page.LCS.elements in
    set_path path ;
    refresh components_to_update app in
  List.iter (fun path -> add_method ("show_" ^ path) (fun_to_add path)) paths

let open_functori _ = set_path ~overwrite:true "https://functori.com/"

let reload_with_current_path () =
  let current_path = get_path () in
  Dom_html.window##.location##replace (Js_of_ocaml.Js.string current_path)
