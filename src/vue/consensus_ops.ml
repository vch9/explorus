(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_process.Consensus
open Tezos.Entities

(* ================================= TYPES ================================= *)

module HashMap = Lib_js.Jsoo.Map.Make (Hash) (HashMap)

module Operations = struct
  module Status = struct
    type t = Operations.Status.t =
      | Applied
      | Outdated
    [@@deriving jsoo { enum }]
  end

  type op = Operations.op = {
    status : Status.t;
    timestamp : float;
    delta : float option;
  }
  [@@deriving jsoo]

  type t = op HashMap.t [@@deriving jsoo]

  let merge = Operations.merge
end

module Ballots = struct
  include Ballots

  type t = Ballots.t = {
    counter : int;
    percent_progression : float;
    consensus : bool;
  }
  [@@deriving jsoo]
end

module BlockInfo = struct
  include BlockInfo

  type t = BlockInfo.t = {
    hash : string;
    timestamp : string;
    time : string;
    received : float;
    delta : float;
  }
  [@@deriving jsoo]
end

module BakerInfo = struct
  include BakerInfo

  type t = BakerInfo.t = {
    endorsing_power : int;
    endorsing_power_percent : float;
    is_missing : bool;
  }
  [@@deriving jsoo]

  let compare_by_missing bi1 bi2 = compare bi1.is_missing bi2.is_missing

  let compare_by_endorsing_power bi1 bi2 =
    compare bi2.endorsing_power bi1.endorsing_power
end

module BakerMap = Lib_js.Jsoo.Map.Make (Baker) (BakerMap)

module RoundMap = struct
  include Lib_js.Jsoo.Native.Map.Make (Round) (RoundMap)

  let reverse_jsoo_conv _a_conv_jsoo =
    let compare (round1, _) (round2, _) = Round.compare round2 round1 in
    sorted_jsoo_conv ~compare _a_conv_jsoo
end

module RoundSet = struct
  include Lib_js.Jsoo.Native.Set.Make (Round) (RoundSet)

  let reverse_jsoo_conv =
    let compare round1 round2 = Int.compare round2 round1 in
    sorted_jsoo_conv ~compare
end

module LevelMap = struct
  include Lib_js.Jsoo.Native.Map.Make (Level) (LevelMap)

  let reverse_jsoo_conv _a_conv_jsoo =
    let compare (level1, _) (level2, _) = Int.compare level2 level1 in
    sorted_jsoo_conv ~compare _a_conv_jsoo
end

type operations = Operations.t EndoPreendo.t [@@deriving jsoo]

type newest_tab_baker = BakerInfo.t * operations [@@deriving jsoo]

let sorted_newest_tabs_jsoo_conv =
  let compare (_baker1, (baker_info1, _operation1))
      (_baker2, (baker_info2, _operation2)) =
    match BakerInfo.compare_by_missing baker_info1 baker_info2 with
    | 0 -> BakerInfo.compare_by_endorsing_power baker_info1 baker_info2
    | i -> i in
  BakerMap.sorted_jsoo_conv ~compare newest_tab_baker_jsoo_conv

type newest_tab_bakers =
  (newest_tab_baker BakerMap.t[@jsoo.conv sorted_newest_tabs_jsoo_conv])
[@@deriving jsoo]

type newest_tab = {
  nt_block_info : BlockInfo.t option;
  nt_level : int;
  nt_round : int;
  nt_voting_state : Ballots.t EndoPreendo.t;
  nt_baker_tabs : newest_tab_bakers;
}
[@@deriving jsoo]

type rounds_operations =
  (operations RoundMap.t
  [@jsoo.conv RoundMap.reverse_jsoo_conv operations_jsoo_conv])
[@@deriving jsoo]

type levels_rounds_operations =
  (rounds_operations LevelMap.t
  [@jsoo.conv LevelMap.reverse_jsoo_conv rounds_operations_jsoo_conv])
[@@deriving jsoo]

type consensus_tabs_baker = BakerInfo.t * levels_rounds_operations
[@@deriving jsoo]

let sorted_consensus_tabs_jsoo_conv =
  let compare (_baker1, (baker_info1, _operation1))
      (_baker2, (baker_info2, _operation2)) =
    BakerInfo.compare_by_missing baker_info1 baker_info2 in
  BakerMap.sorted_jsoo_conv ~compare consensus_tabs_baker_jsoo_conv

type consensus_tabs_bakers =
  (consensus_tabs_baker BakerMap.t[@jsoo.conv sorted_consensus_tabs_jsoo_conv])
[@@deriving jsoo]

type all_round = (RoundSet.t[@jsoo.conv RoundSet.reverse_jsoo_conv])
[@@deriving jsoo]

type all_level_round =
  (all_round LevelMap.t
  [@jsoo.conv LevelMap.reverse_jsoo_conv all_round_jsoo_conv])
[@@deriving jsoo]

type consensus_tabs = {
  ct_baker_tabs : consensus_tabs_bakers;
  ct_all_level_round : all_level_round;
}
[@@deriving jsoo]

type data = {
  newest_tab : newest_tab option; [@mutable]
  other_tabs : consensus_tabs option; [@mutable]
  protocol_transition_level : int;
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-consensus_ops"

  let element =
    Vue_component.CRender
      (Render.consensus_ops_render, Render.consensus_ops_static_renders)

  let props = None
end)

(* ============================ DYNAMIC DISPLAY ============================ *)

let get_max_level_round_consensus_state consensus_states =
  Option.bind (LevelMap.max_binding_opt consensus_states)
  @@ fun (level, LevelInfo.{ rounds; bakers_info; _ }) ->
  RoundMap.max_binding_opt rounds
  |> Option.map @@ fun (round, state) -> (level, round, bakers_info, state)

let merge_bakers_info_and_baker_map_to_baker_tabs :
    empty:'a ->
    BakerInfo.t BakerMap.t ->
    'a BakerMap.t ->
    (BakerInfo.t * 'a) BakerMap.t =
  let empty_baker_info =
    BakerInfo.
      { endorsing_power = 0; endorsing_power_percent = 0.; is_missing = false }
  in
  fun ~empty bakers_info baker_map ->
    BakerMap.merge
      (fun _baker baker_info tab ->
        let baker_info = Option.value ~default:empty_baker_info baker_info in
        let tab = Option.value ~default:empty tab in
        Some (baker_info, tab))
      bakers_info baker_map

let baker_map_endo_preendo_to_endo_preendo_baker_map :
    empty:'a -> 'a BakerMap.t EndoPreendo.t -> 'a EndoPreendo.t BakerMap.t =
 fun ~empty EndoPreendo.{ endorsement; preendorsement } ->
  BakerMap.merge
    (fun _baker endorsement preendorsement ->
      let endorsement = Option.value ~default:empty endorsement in
      let preendorsement = Option.value ~default:empty preendorsement in
      Some EndoPreendo.{ endorsement; preendorsement })
    endorsement preendorsement

let get_bakers_ops votings : Operations.t EndoPreendo.t BakerMap.t =
  let bakers = EndoPreendo.map (fun Votings.{ bakers; _ } -> bakers) votings in
  baker_map_endo_preendo_to_endo_preendo_baker_map ~empty:HashMap.empty bakers

let get_newest_tab (level, round, bakers_info, State.{ votings; block_info; _ })
    =
  let nt_voting_state =
    EndoPreendo.map (fun Votings.{ ballots; _ } -> ballots) votings in
  let bakers_ops = get_bakers_ops votings in
  let nt_baker_tabs =
    merge_bakers_info_and_baker_map_to_baker_tabs
      ~empty:(EndoPreendo.init HashMap.empty)
      bakers_info bakers_ops in
  {
    nt_block_info = block_info;
    nt_level = level;
    nt_round = round;
    nt_voting_state;
    nt_baker_tabs;
  }

let newest_tab_connection ~app =
  let refresh_app _ =
    let consensus_states =
      ConcurrentValue.get Tezos_process.ConsensusStates.all_states in
    get_max_level_round_consensus_state consensus_states
    |> Option.iter @@ fun newest_consensus_state ->
       let newest_tab = get_newest_tab newest_consensus_state in
       app##.newest_tab_ := def @@ newest_tab_to_jsoo newest_tab in
  let refresh_app =
    Periodic_bundled_apply.make ~sleep:EzLwtSys.sleep ~delay:1. refresh_app
  in
  Listener.connect Tezos_process.ConsensusStates.update_listener refresh_app

let get_consensus_states_without_level_round ~level ~round =
  LevelMap.update level @@ function
  | None -> None
  | Some level_info ->
    let rounds = RoundMap.remove round level_info.LevelInfo.rounds in
    if RoundMap.is_empty rounds then
      None
    else
      Some LevelInfo.{ level_info with rounds }

let get_relevant_consensus_states_from_penultimate_level_round consensus_states
    =
  Option.bind (get_max_level_round_consensus_state consensus_states)
  @@ fun (max_level, max_round, _, _) ->
  let consensus_states =
    get_consensus_states_without_level_round ~level:max_level ~round:max_round
      consensus_states in
  match LevelMap.rev_bindings consensus_states with
  | [] -> None
  | (level, level_info) :: others_consensus_states ->
    let rounds =
      RoundMap.take_last Konstant.first_old_level_consensus_tabs_displayed
        level_info.LevelInfo.rounds in
    let consensus_states =
      LevelMap.singleton level LevelInfo.{ level_info with rounds } in
    let consensus_states =
      match others_consensus_states with
      | [] -> consensus_states
      | (level, level_info) :: _ ->
        let rounds =
          RoundMap.take_last Konstant.old_level_consensus_tabs_displayed
            level_info.LevelInfo.rounds in
        LevelMap.add level LevelInfo.{ level_info with rounds } consensus_states
    in
    Some consensus_states

let squash_all_bakers_info consensus_states =
  LevelMap.fold
    (fun _level LevelInfo.{ bakers_info; _ } ->
      BakerMap.simple_union (fun _baker -> BakerInfo.merge) bakers_info)
    consensus_states BakerMap.empty

let baker_round_level_map_to_round_level_baker_map :
    'a BakerMap.t RoundMap.t LevelMap.t -> 'a RoundMap.t LevelMap.t BakerMap.t =
 fun baker_round_level_map ->
  ( LevelMap.fold @@ fun level ->
    RoundMap.fold @@ fun round ->
    BakerMap.fold @@ fun baker x ->
    BakerMap.update_with_default baker ~default:LevelMap.empty
    @@ LevelMap.update_with_default level ~default:RoundMap.empty
    @@ RoundMap.add round x )
    baker_round_level_map BakerMap.empty

let to_round_level_map f =
  LevelMap.map @@ fun LevelInfo.{ rounds; bakers_info; _ } ->
  rounds |> RoundMap.map (f bakers_info)

let to_round_set_level_map =
  LevelMap.map @@ fun LevelInfo.{ rounds; _ } ->
  RoundMap.fold (fun round _ -> RoundSet.add round) rounds RoundSet.empty

let get_other_tabs ~all_bakers_info consensus_states =
  let all_empty_operations_round_level_map =
    to_round_level_map
      (fun _ _ -> EndoPreendo.init HashMap.empty)
      consensus_states in
  let all_bakers_ops =
    to_round_level_map
      (fun _ State.{ votings; _ } -> get_bakers_ops votings)
      consensus_states in
  let bakers_ops =
    baker_round_level_map_to_round_level_baker_map all_bakers_ops in
  let filled_bakers_ops =
    BakerMap.map
      (( LevelMap.simple_union @@ fun _level ->
         RoundMap.simple_union @@ fun _round ->
         EndoPreendo.merge Operations.merge )
         all_empty_operations_round_level_map)
      bakers_ops in
  let ct_baker_tabs =
    merge_bakers_info_and_baker_map_to_baker_tabs
      ~empty:all_empty_operations_round_level_map all_bakers_info
      filled_bakers_ops in
  let ct_all_level_round = to_round_set_level_map consensus_states in
  { ct_baker_tabs; ct_all_level_round }

let other_tabs_connection ~app =
  let refresh_app _ =
    let consensus_states =
      ConcurrentValue.get Tezos_process.ConsensusStates.all_states in
    let all_bakers_info = squash_all_bakers_info consensus_states in
    get_relevant_consensus_states_from_penultimate_level_round consensus_states
    |> Option.iter @@ fun relevant_old_consensus_states ->
       let other_tabs =
         get_other_tabs ~all_bakers_info relevant_old_consensus_states in
       app##.other_tabs_ := def @@ consensus_tabs_to_jsoo other_tabs in
  let refresh_app =
    Periodic_bundled_apply.make ~sleep:EzLwtSys.sleep ~delay:1. refresh_app
  in
  Listener.connect Tezos_process.ConsensusStates.update_listener refresh_app

let init ~protocol_transition_level () =
  let data _ =
    object%js
      val mutable newest_tab_ = undefined

      val mutable other_tabs_ = undefined

      val protocol_transition_level_ = protocol_transition_level
    end in
  let at_destroy = PendingFunctions.create () in
  C.make
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            let newest_tab_connection_id = newest_tab_connection ~app in
            let other_tabs_connection_id = other_tabs_connection ~app in
            PendingFunctions.add at_destroy @@ fun () ->
            Listener.disconnect newest_tab_connection_id ;
            Listener.disconnect other_tabs_connection_id );
        ("destroyed", fun _app -> PendingFunctions.run at_destroy ());
      ]
    ~data ()
