(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Rpc

(* ================================= TYPES ================================= *)

type delegate_info = {
  phk : string;
  alias : string;
  current_stake : float;
  endorsement_weight : float;
  expected_activity : int;
  expected_minimal_activity : int;
  number_of_missed_opportunities : int;
  allowed_missed_slots_remaining : int;
  missed_percentage : float;
  expected_reward : float;
  frozen_deposit : float;
  number_of_delegates : int;
  is_deactivated : string;
  grace_period_cycle : int;
}
[@@deriving jsoo]

type delegates_infos = delegate_info list [@@deriving jsoo]

type field = {
  key : string;
  label : string;
  descr : string;
  sortable : bool;
}
[@@deriving jsoo]

type fields = field list [@@deriving jsoo]

type data = {
  delegates_infos : delegates_infos option; [@mutable]
  current_page : int;
  per_page : int;
  fields : fields;
  genesis : bool; [@mutable]
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-bakers_activity"

  let element =
    Vue_component.CRender
      (Render.bakers_activity_render, Render.bakers_activity_static_renders)

  let props = None
end)

(* ============================ ENDORSEMENT OPS ============================ *)

module EndorsingOps : sig
  val clean_total_activity : unit -> unit

  val get_total_activity : unit -> int

  val get_endorsement_weight : int -> float

  val update_total_activity : int -> unit

  val set_all_endorsement_weights : delegate_info list -> delegate_info list

  val sort_by_endorsement_weights : delegate_info list -> delegate_info list
end = struct
  let total_activity = ref 0

  let clean_total_activity () = total_activity := 0

  let get_total_activity () = !total_activity

  let update_total_activity delegate_expected_cycle_activity =
    total_activity := delegate_expected_cycle_activity + !total_activity

  let get_endorsement_weight expected_cycle_activity =
    Lib_tz.endorsement_weight expected_cycle_activity !total_activity

  let set_all_endorsement_weights =
    List.map (fun d_i ->
        {
          d_i with
          endorsement_weight =
            get_endorsement_weight @@ Float.to_int d_i.endorsement_weight;
        })

  let sort_by_endorsement_weights =
    List.sort (fun d_i_1 d_i_2 ->
        compare d_i_2.endorsement_weight d_i_1.endorsement_weight)
end

(* ============================= CONSTRUCTIONS ============================= *)

let construct_delegate_info delegate =
  let%lwt participation =
    Cache.Participations.get Central_state.Cache.participations delegate
      ~search_for:(get_participation_and_ts delegate) in
  EndorsingOps.update_total_activity participation.pr_expected_cycle_activity ;
  let%lwt info =
    Cache.DelegatesInfo.get Central_state.Cache.delegates_info delegate
      ~search_for:(get_delegate_info_and_ts delegate) in
  let%lwt alias = get_alias_lwt delegate in
  let current_stake = Lib_tz.mutez_to_tez info.di_staking_balance in
  let expected_reward =
    Lib_tz.mutez_to_tez participation.pr_expected_endorsing_rewards in
  let frozen_deposit = Lib_tz.mutez_to_tez info.di_frozen_deposits in
  let number_of_missed_opportunities =
    Float.to_int
    @@ Lib_tz.missed_slots participation.pr_expected_cycle_activity
         participation.pr_minimal_cycle_activity
         participation.pr_remaining_allowed_missed_slots in
  let missed_percentage =
    Lib_tz.percent_missed participation.pr_expected_cycle_activity
      participation.pr_minimal_cycle_activity
      participation.pr_remaining_allowed_missed_slots in
  let number_of_delegates = List.length info.di_delegated_contracts in
  Lwt.return
    {
      phk = delegate;
      alias = Option.value ~default:Pretty.none_str alias;
      current_stake;
      endorsement_weight = Int.to_float participation.pr_expected_cycle_activity;
      expected_activity = participation.pr_expected_cycle_activity;
      expected_minimal_activity = participation.pr_minimal_cycle_activity;
      number_of_missed_opportunities;
      missed_percentage;
      expected_reward;
      frozen_deposit;
      allowed_missed_slots_remaining =
        participation.pr_remaining_allowed_missed_slots;
      number_of_delegates;
      is_deactivated = Pretty.bool_to_interjection info.di_deactivated;
      grace_period_cycle = info.di_grace_period;
    }

let construct_delegates_info delegates =
  Lwt_list.map_p construct_delegate_info delegates

(* =========================== REFRESH MECHANISMS =========================== *)

module Refresh = struct
  let trigger = SafeTrigger.create ()
end

let charge_all_bakers_activity_infos app =
  Cache.Participations.clean_routine Central_state.Cache.participations ;
  Cache.DelegatesInfo.clean_routine Central_state.Cache.delegates_info ;
  Lwt.async @@ fun _ ->
  EndorsingOps.clean_total_activity () ;
  let%lwt () =
    ConcurrentValue.update Central_state.Cache.delegates @@ fun delegates ->
    if List.is_empty delegates then
      get_delegates ()
    else
      Lwt.return delegates in
  let delegates = ConcurrentValue.get Central_state.Cache.delegates in
  let%lwt delegates_info = construct_delegates_info delegates in
  let delegates_info =
    EndorsingOps.sort_by_endorsement_weights
    @@ EndorsingOps.set_all_endorsement_weights delegates_info in
  app##.delegates_infos_ := def (delegates_infos_to_jsoo delegates_info) ;
  Lwt.return_unit

let update_genesis app is_genesis = app##.genesis := is_genesis

let refresh_endorsing_table app =
  let callback () = charge_all_bakers_activity_infos app in
  let open Central_state.Intervals in
  add_intervals ~clearable_type:Unclearable ~callback
    ~loop_frequency:Konstant.endorsing_refresh_cyle_duration

let init () =
  let data _ =
    object%js
      val mutable delegates_infos_ = undefined

      val current_page_ = 1

      val per_page_ = 15

      val mutable genesis = Js_of_ocaml.Js._false

      val fields =
        fields_to_jsoo
          [
            {
              key = "phk";
              label = "Delegate";
              sortable = false;
              descr = "A 'tz' account registered as a baker/validator";
            };
            {
              key = "alias";
              label = "Alias";
              sortable = false;
              descr =
                "Who's baking with that 'tz' account ([none] means that there \
                 iscurrently no known alias for this account)";
            };
            {
              key = "current_stake";
              label = "C.Stake";
              sortable = true;
              descr =
                "Current stake of the delegate (the one that will be \
                 snapshotted for rights in future cycles)";
            };
            {
              key = "endorsement_weight";
              label = "E.Weight";
              sortable = false;
              descr = "Endorsement weight (percentage) in this cycle";
            };
            {
              key = "expected_activity";
              label = "A(exp)";
              sortable = false;
              descr =
                "Expected activity (endorsements), or nb of attributed slots";
            };
            {
              key = "expected_minimal_activity";
              label = "A(min)";
              sortable = false;
              descr = "Expected minimal activity to get endorsing rewards";
            };
            {
              key = "number_of_missed_opportunities";
              label = "A(miss)";
              sortable = false;
              descr = "Number of already missed opportunities";
            };
            {
              key = "allowed_missed_slots_remaining";
              label = "A(rem)";
              sortable = false;
              descr =
                "Number of remaining allowed misses before loosing rewards";
            };
            {
              key = "missed_percentage";
              label = "Missed";
              sortable = false;
              descr = "Percentage of missed opportunities (bounded by 1/3)";
            };
            {
              key = "expected_reward";
              label = "Exp. rew.";
              sortable = false;
              descr = "Expected endorsing rewards at the end of the cycle";
            };
            {
              key = "frozen_deposit";
              label = "F.Deposit";
              sortable = false;
              descr = "Current amount of frozen security deposits";
            };
            {
              key = "number_of_delegates";
              label = "Deleg. nb.";
              sortable = false;
              descr = "Number of delegations to this baker (including himself)";
            };
            {
              key = "is_deactivated";
              label = "Is deact. ?";
              sortable = false;
              descr = "Give info about if the baker is deactivated";
            };
            {
              key = "grace_period_cycle";
              label = "Grace per.";
              sortable = false;
              descr =
                "The grace period is the cycle at which the baker will be \
                 deactivated if he/she/it stops participating";
            };
          ]
    end in
  let load_bakers_activity app =
    charge_all_bakers_activity_infos app ;
    if not @@ SafeTrigger.access Refresh.trigger then begin
      SafeTrigger.trigger Refresh.trigger ;
      refresh_endorsing_table app
    end in
  C.make ~data
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            Lwt.async @@ fun () ->
            let%lwt block = Rpc.get_block () in
            if block.bl_header.hd_level > Konstant.genesis_level then (
              load_bakers_activity app ;
              Lwt.return_unit
            ) else (
              update_genesis app Js_of_ocaml.Js._true ;
              Lwt.return_unit
            ) );
      ]
    ()
