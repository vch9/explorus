(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open SORU

type data = {
  id : int;
  processed : bool; [@mutable]
  info : L1.commitment_infos option; [@mutable]
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  open Vue_component

  class type data = data_jsoo

  let name = "v-soru_commitment"

  let element =
    CRender
      (Render.soru_commitment_render, Render.soru_commitment_static_renders)

  let make_prop_object ?required ?validator default =
    PrObj
      { pr_default = default; pr_required = required; pr_validator = validator }

  let props = Some (PrsObj [("hash", make_prop_object (JString "default"))])

  type props = { hash : string } [@@deriving jsoo]

  class type all =
    object
      inherit data

      inherit props_jsoo
    end
end)

let get_info app =
  let get_info () =
    let get_info (module Smart_rollup : Tezos_process.SORU.L1.SMART_ROLLUP) =
      Lwt.async @@ fun () ->
      let hash = to_string app##.hash in
      app##.processed := _false ;
      let%lwt info = Smart_rollup.get_commitment_infos hash in
      let info = Optdef.(map (option info) L1.commitment_infos_to_jsoo) in
      app##.info := info ;
      app##.processed := _true ;
      Lwt.return_unit in
    let smart_rollup =
      ConcurrentValue.get Tezos_process.SORU.L1.current_smart_rollup in
    Option.iter get_info smart_rollup in
  Optdef.case app##.info get_info (fun _ -> ())

let init () =
  let gen_id = GenId.create () in
  let data _ =
    object%js
      val id = GenId.gen gen_id

      val mutable processed = _false

      val mutable info = undefined
    end in
  C.add_method0 "get_info" get_info ;
  C.make ~data ()
