(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Rpc

(* ================================= TYPES ================================= *)

type cycle_infos = {
  current_cycle : string;
  current_cycle_position : string;
  k_blocks_per_cycle : string;
  progression_percentage : string;
}
[@@deriving jsoo]

type block_timestamp = {
  time : string;
  delta : float;
}
[@@deriving jsoo]

type data = {
  block : block option; [@mutable]
  block_timestamp : block_timestamp option; [@mutable]
  cycle_infos : cycle_infos option; [@mutable]
  node_version : node_version option; [@mutable]
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all = data

  let name = "v-info_bar"

  let element =
    Vue_component.CRender
      (Render.info_bar_render, Render.info_bar_static_renders)

  let props = None
end)

(* ============================= CONSTRUCTIONS ============================= *)

let charge_node_version app =
  Lwt.async @@ fun () ->
  let%lwt node_version = get_node_version () in
  app##.node_version_ := def (node_version_to_jsoo node_version) ;
  Lwt.return_unit

let get_cycle_infos block network_constants =
  let metadata =
    Option.value ~default:Tezos_process.default_metadata block.bl_metadata in
  {
    current_cycle =
      (match metadata.meta_level_info with
      | None -> Pretty.none_str
      | Some level_info -> Int.to_string level_info.cycle);
    current_cycle_position =
      (match metadata.meta_level_info with
      | None -> Pretty.none_str
      | Some level_info -> Int.to_string level_info.cycle_position);
    k_blocks_per_cycle = Int.to_string network_constants.nc_blocks_per_cycle;
    progression_percentage =
      (match metadata.meta_level_info with
      | None -> Pretty.none_str
      | Some level_info ->
        Float.to_string
        @@ Lib_tz.progression_percentage
             (Int.to_float level_info.cycle_position)
             (Int.to_float network_constants.nc_blocks_per_cycle));
  }

(* =========================== REFRESH MECHANISMS =========================== *)

let charge_blockchain_info_vue app block =
  app##.block := def @@ block_to_jsoo block ;
  let network_constants = Central_state.Network.get_network_constants () in
  let cycle_infos = get_cycle_infos block network_constants in
  app##.cycle_infos_ := def (cycle_infos_to_jsoo cycle_infos) ;
  Lwt.return_unit

let charge_head_timestamp_vue app ~block_stamped =
  let Tezos_process.TS.{ block; ts_infos } = block_stamped in
  let time, _ = Lib_tz.parse_tezos_time_date block.bl_header.hd_timestamp in
  let timestamp = ts_infos.Tezos_process.TS.proposed in
  let delta = ts_infos.Tezos_process.TS.received -. timestamp in
  app##.block_timestamp_ := def @@ block_timestamp_to_jsoo { time; delta } ;
  Lwt.return_unit

let charge_info_bar app heads =
  Lwt.async @@ fun () ->
  Lwt_list.iter_s
    (fun block_stamped ->
      let%lwt () =
        charge_blockchain_info_vue app block_stamped.Tezos_process.TS.block
      in
      charge_head_timestamp_vue app ~block_stamped)
    heads

let init () =
  let data _ =
    object%js
      val mutable block = undefined

      val mutable cycle_infos_ = undefined

      val mutable node_version_ = undefined

      val mutable block_timestamp_ = undefined
    end in
  let make_preliminaries app = charge_node_version app in
  C.make ~data
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            let () = make_preliminaries app in
            Listener.connect ~use_storage:true Tezos_process.heads_listener
              (charge_info_bar app)
            |> ignore );
      ]
    ()
