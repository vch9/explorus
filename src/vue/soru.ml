(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open SORU
module L1_Rpc = Rpc.SORU.L1
open Tezos.Entities

type commitment_tree = Tezos_process.SORU.L1.Commitment_tree.t = {
  commitment : string;
  stakers : string list;
  childs : commitment_tree list;
}
[@@deriving jsoo { recursive = true }]

type staker_info = Tezos_process.SORU.L1.staker_info = {
  last_commitment : L1.commitment_infos_with_hash option;
  id : L1.staker_id;
}
[@@deriving jsoo]

module StakerMap = Lib_js.Jsoo.Map.Make (Staker) (StakerMap)

let sorted_staker_info_jsoo_conv =
  let compare (staker1, staker_info1) (staker2, staker_info2) =
    match (staker_info1.last_commitment, staker_info2.last_commitment) with
    | None, Some _ -> 1
    | Some _, None -> -1
    | None, None -> Staker.compare staker1 staker2
    | Some lc1, Some lc2 ->
      Compute.lexico_compare
        L1.
          [
            Int.compare lc2.cih_info.inbox_level lc1.cih_info.inbox_level;
            String.compare lc1.cih_hash lc2.cih_hash;
            Staker.compare staker1 staker2;
          ] in
  StakerMap.sorted_jsoo_conv ~compare staker_info_jsoo_conv

type stakers_info =
  (staker_info StakerMap.t[@jsoo.conv sorted_staker_info_jsoo_conv])
[@@deriving jsoo]

module StakerKey = struct
  include Tezos_process.SORU.L1.StakerKey

  type t = L1.staker * L1.staker_id [@@deriving jsoo]
end

module Conflict = struct
  include Tezos_process.SORU.L1.Conflict

  type nonrec t = t = {
    inbox_level : int;
    staker1 : StakerKey.t;
    staker2 : StakerKey.t;
  }
  [@@deriving jsoo]
end

module ConflictSet =
  Lib_js.Jsoo.Set.Make (Conflict) (Tezos_process.SORU.L1.ConflictSet)

type conflicts = ConflictSet.t [@@deriving jsoo]

module LevelMap = Lib_js.Jsoo.Native.Map.Make (Level) (LevelMap)

module Game = struct
  include Tezos_process.SORU.L1.Game

  module Id = struct
    include Id

    type nonrec t = t = {
      inbox_level : int;
      start_level : int;
      alice : L1.staker;
      bob : L1.staker;
    }
    [@@deriving jsoo]
  end

  module IdMap = Lib_js.Jsoo.Map.Make (Id) (IdMap)

  module Turn = struct
    include Turn

    type nonrec t = t = {
      turn : L1.player;
      game_state : L1.game_state;
      timeout : L1.timeout;
      last_agree_tick : int option;
    }
    [@@deriving jsoo]
  end

  type nonrec ending = ending = {
    final_level : int;
    refutation : L1.refutation option;
    game_result : L1.game_result;
  }
  [@@deriving jsoo]

  type nonrec t = t = {
    conflict : Conflict.t;
    turns : Turn.t LevelMap.t;
    last_timeout : L1.timeout;
    ending : ending option;
  }
  [@@deriving jsoo]
end

type games = Game.t Game.IdMap.t [@@deriving jsoo]

type commitment_tree_context = {
  commitment_tree : commitment_tree option; [@mutable]
  update : unit -> unit;
  play : unit -> unit;
  pause : unit -> unit;
  is_playing : bool; [@mutable]
  is_running : bool; [@mutable]
}
[@@deriving jsoo]

type l1_id_infos = {
  genesis_info : L1.genesis_info;
  kind : L1.kind;
  last_cemented_commitment : L1.commitment_infos_with_hash; [@mutable]
  stakers_info : stakers_info option; [@mutable]
  conflicts : conflicts option; [@mutable]
  games : games option; [@mutable]
  commitment_tree_context : commitment_tree_context; [@mutable]
}
[@@deriving jsoo]

type l1_gen_infos = {
  inbox : L1.rollups_inbox;
  soru_addresses : L1.soru_addresses;
}
[@@deriving jsoo]

type soru_context = { id : string } [@@deriving jsoo]

type pending_ops = {
  po_general_infos : bool;
  po_id_infos : bool;
  po_commitment_tree : bool;
}
[@@deriving jsoo]

type form_input_ids = { soru_id : string } [@@deriving jsoo]

type data = {
  l1_gen_infos : l1_gen_infos option; [@mutable]
  l1_id_infos : l1_id_infos option; [@mutable]
  soru_context : soru_context option; [@mutable]
  pending_ops : pending_ops; [@mutable]
}
[@@deriving jsoo { remove_prefix = false }]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-soru"

  let element =
    Vue_component.CRender (Render.soru_render, Render.soru_static_renders)

  let props = None
end)

let pending_ops ~app ?general_infos ?id_infos ?commitment_tree () =
  let pending_ops = pending_ops_of_jsoo app##.pending_ops_ in
  let po_general_infos =
    Option.value ~default:pending_ops.po_general_infos general_infos in
  let po_id_infos = Option.value ~default:pending_ops.po_id_infos id_infos in
  let po_commitment_tree =
    Option.value ~default:pending_ops.po_commitment_tree commitment_tree in
  let pending_ops = { po_general_infos; po_id_infos; po_commitment_tree } in
  app##.pending_ops_ := pending_ops_to_jsoo pending_ops

let set_l1_gen_infos ~at_destroy ~app =
  let open Protocol.Infix in
  if Central_state.Protocol.get_current () >= Protocol.MUMBAI then begin
    pending_ops ~app ~general_infos:true () ;
    let connection =
      Listener.connect Tezos_process.heads_listener @@ fun _ ->
      Lwt.async @@ fun () ->
      let%lwt inbox = L1_Rpc.get_inbox () in
      let%lwt soru_addresses = L1_Rpc.get_soru_addresses () in
      let l1_gen_infos = { soru_addresses; inbox } in
      app##.l1_gen_infos_ := def @@ l1_gen_infos_to_jsoo l1_gen_infos ;
      pending_ops ~app ~general_infos:false () ;
      Lwt.return_unit in
    PendingFunctions.add at_destroy (fun () -> Listener.disconnect connection)
  end

let get_commitment_tree_context ~app get_commitment_tree =
  let at_pause = PendingFunctions.create () in
  let commitment_tree = None in
  let with_context f =
    Optdef.iter app##.l1_id_infos_ @@ fun l1_id_infos ->
    f l1_id_infos##.commitment_tree_context_ in
  let set ctxt =
    ctxt##.is_running_ := _true ;
    let%lwt commitment_tree = get_commitment_tree () in
    ctxt##.commitment_tree_ := def @@ commitment_tree_to_jsoo commitment_tree ;
    ctxt##.is_running_ := _false ;
    Lwt.return_unit in
  let update () =
    with_context @@ fun ctxt ->
    Lwt.async @@ fun () -> set ctxt in
  let play () =
    with_context @@ fun ctxt ->
    let paused, pause = Lwt.wait () in
    ctxt##.is_playing_ := _true ;
    let connection =
      Listener.connect Tezos_process.heads_listener @@ fun _ ->
      Lwt.async @@ fun () -> Lwt.pick [set ctxt; paused] in
    PendingFunctions.add at_pause (fun () ->
        Listener.disconnect connection ;
        Lwt.wakeup pause ()) in
  let pause () =
    with_context @@ fun ctxt ->
    PendingFunctions.run at_pause () ;
    ctxt##.is_running_ := _false ;
    ctxt##.is_playing_ := _false in
  let is_playing = false in
  let is_running = false in
  { commitment_tree; update; play; pause; is_playing; is_running }

let set_l1_id_infos ~soru_close app smart_rollup_id =
  PendingFunctions.run soru_close () ;
  let%lwt (module Smart_rollup : Tezos_process.SORU.L1.SMART_ROLLUP) =
    Tezos_process.SORU.L1.set ~smart_rollup_id in
  let genesis_info = Smart_rollup.genesis_info in
  let kind = Smart_rollup.kind in
  let%lwt last_cemented_commitment = Listener.get Smart_rollup.lcc_listener in
  let stakers_info = None in
  let conflicts = None in
  let games = None in
  let commitment_tree_context =
    get_commitment_tree_context ~app Smart_rollup.get_commitment_tree in
  let l1_id_infos =
    {
      genesis_info;
      kind;
      last_cemented_commitment;
      stakers_info;
      conflicts;
      games;
      commitment_tree_context;
    } in
  let l1_id_infos = l1_id_infos_to_jsoo l1_id_infos in
  app##.l1_id_infos_ := def l1_id_infos ;
  commitment_tree_context.update () ;
  let lcc_update_connection =
    Listener.connect Smart_rollup.lcc_listener @@ fun lcc ->
    l1_id_infos##.last_cemented_commitment_
    := L1.commitment_infos_with_hash_to_jsoo lcc in
  let stakers_info_update_connection =
    Listener.connect Smart_rollup.stakers_info_listener @@ fun stakers_info ->
    l1_id_infos##.stakers_info_ := def @@ stakers_info_to_jsoo stakers_info
  in
  let conflicts_update_connection =
    Listener.connect Smart_rollup.conflicts_listener @@ fun conflicts ->
    l1_id_infos##.conflicts := def @@ conflicts_to_jsoo conflicts in
  let games_update_connection =
    Listener.connect Smart_rollup.games_listener @@ fun games ->
    l1_id_infos##.games := def @@ games_to_jsoo games in
  PendingFunctions.add soru_close (fun () ->
      Listener.disconnect lcc_update_connection ;
      Listener.disconnect stakers_info_update_connection ;
      Listener.disconnect conflicts_update_connection ;
      Listener.disconnect games_update_connection) ;
  Lwt.return_unit

let set_context ~soru_close app id =
  pending_ops ~app ~id_infos:true () ;
  let%lwt valid_id = L1_Rpc.get_id_validity_test id in
  if valid_id then (
    app##.soru_context_ := def @@ soru_context_to_jsoo { id } ;
    let%lwt () = set_l1_id_infos ~soru_close app id in
    pending_ops ~app ~id_infos:false () ;
    Lwt.return_unit
  ) else (
    pending_ops ~app ~id_infos:false () ;
    Lwt.return_unit
  )

let get_form_values ~soru_close app selected =
  let open Protocol.Infix in
  if Central_state.Protocol.get_current () >= Protocol.MUMBAI then
    Lwt.async @@ fun () ->
    try set_context ~soru_close app @@ Js_of_ocaml.Js.to_string selected
    with _ -> Lwt.return_unit

let init () =
  let data _ =
    object%js
      val mutable l1_gen_infos_ = undefined

      val mutable l1_id_infos_ = undefined

      val mutable soru_context_ = undefined

      val mutable pending_ops_ =
        pending_ops_to_jsoo
          {
            po_general_infos = false;
            po_id_infos = false;
            po_commitment_tree = false;
          }
    end in
  let soru_close = PendingFunctions.create () in
  let at_destroy = PendingFunctions.(create ~functions:[run soru_close] ()) in
  C.add_method1 "get_form_values" (get_form_values ~soru_close) ;
  C.make ~data
    ~lifecycle:
      [
        ("beforeMount", fun app -> set_l1_gen_infos ~at_destroy ~app);
        ( "destroyed",
          fun _app ->
            PendingFunctions.run at_destroy () ;
            Lwt.async Tezos_process.SORU.L1.remove );
      ]
    ()
