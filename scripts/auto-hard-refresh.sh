#!/bin/sh

# ====================== /!\ DO NOT CHANGE THOSE LINES /!\ ====================== #
# This script allows the server to auto-hard-refresh every time a deploy is made. #

head_hash="git rev-parse HEAD"
hash=$(eval "$head_hash")
sed_line="20s#.*#        <script src=\"/explorus.js\?version=${hash}\" defer></script>#"
sed -i "${sed_line}" "static/index.html"
