#!/bin/sh

JSON_FILE="json/parameters.json"
echo "Enter your own node address for Dailynet:"
read EXPLORUS_NODE_ADDRESS
TEZTNETS_NODE_ADDRESS=$(curl -s https://teztnets.xyz/teztnets.json | jq '.[] | select(.human_name == "Dailynet")'.rpc_url | tr -d '"')

echo $(jq '.dailynet.addresses = [$EXPLORUS_NODE_ADDRESS]' --arg EXPLORUS_NODE_ADDRESS $EXPLORUS_NODE_ADDRESS $JSON_FILE) > $JSON_FILE
echo $(jq '.dailynet.addresses += [$TEZTNETS_NODE_ADDRESS]' --arg TEZTNETS_NODE_ADDRESS $TEZTNETS_NODE_ADDRESS $JSON_FILE) > $JSON_FILE 
npx prettier --write $JSON_FILE
