#!/bin/sh

if make build-alcotest ; then
    echo "==> Tests build successfully"
else 
    echo "Failure: tests build failed"
    exit 1
fi

if make exec-alcotest-quick ; then 
    echo "==> Alcotest executed successfully"
else
    echo "Failure: alcotest execution failed"
    exit 1
fi

echo "Success: all tests build and passed"
exit 0
