#!/bin/sh

CURRENT_FOLDER=$PWD

cd /tmp
rm -rf /flextesa
git clone git@gitlab.com:tezos/flextesa.git
cd flextesa
opam switch create . --deps-only --formula='"ocaml-base-compiler" {>= "4.13" & < "4.14"}' --no-install
eval $(opam env)
opam pin add -n tezai-base58-digest https://gitlab.com/oxheadalpha/tezai-base58-digest.git -y
opam install --deps-only --with-test --with-doc ./tezai-tz1-crypto.opam ./flextesa.opam ./flextesa-cli.opam -y
make
cp -f flextesa $CURRENT_FOLDER
cd $CURRENT_FOLDER
rm -rf /tmp/flextesa
