(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Json_types

let components_states_path_ok () =
  Lib_tst.destruct_json_path Paths.components_states components_states_enc

let components_states_encoding () =
  Lib_tst.destruct_json_encoding Paths.components_states components_states_enc

(* In our case, generally we don't want any of the components to be deactivated,
   we have to make sure of it *)
let all_components_are_active () =
  let components_states =
    Lib_tst.destruct_json Paths.components_states components_states_enc in
  components_states.cp_baking_rights && components_states.cp_latest_blocks
  && components_states.cp_consensus_ops && components_states.cp_bakers_activity
  && components_states.cp_info_bar && components_states.cp_consensus_progression
  && components_states.cp_network_constants && components_states.cp_pyrometer
  && components_states.cp_settings

let test_path _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ components_states_path_ok ()

let test_encoding _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ components_states_encoding ()

let test_all_components_are_active _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ all_components_are_active ()
